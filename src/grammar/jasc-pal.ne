@preprocessor typescript

@{%

const moo = require('moo');

let lexer = moo.compile({
	ws: /[ \t]+/,
	nl: { match: /[\r]?[\n]/, lineBreaks: true },
	file_format: "JASC-PAL",
	version_num: "0100",
	pos_integer: {
		match: /[0-9]+/,
		value: (s: string) => Number(s),
	},
});

%}

@lexer lexer

File ->
	FileHeader next_line ColorList _n {% (d) => [d[0], d[2]] %}

FileHeader ->
	%file_format next_line %version_num next_line PosInteger {% (d) => ({ type: "header", format: d[1], version: d[3], numColors: d[5] }) %}

ColorList ->
	_ColorList {% (d) => ({ type: "color-list", colors: d[0] }) %}

_ColorList ->
	RGBTuple {% (d) => [d[0]] %}
    | _ColorList next_line RGBTuple {% (d) => [...d[0], d[2]] %}

RGBTuple ->
	PosInteger __ PosInteger __ PosInteger {% (d) => ({ type: "rgb", r: d[0].value, g: d[2].value, b: d[4].value }) %}

# Primitives

PosInteger ->
	%pos_integer {% id %}

# Whitespace

next_line ->
	_ %nl _ {% () => [] %}

next_present_line -> # "present" is borrowed from ruby, just means non-whitespace
	_ %nl _n {% () => [] %}

multi_line_ws ->
	%ws | %nl {% () => [] %}

_ ->
	%ws:* {% () => [] %}
__ ->
	%ws:+ {% () => [] %}
_n ->
	multi_line_ws:* {% () => [] %}
__n ->
	multi_line_ws:+ {% () => [] %}
