@preprocessor typescript

@{%

const moo = require('moo');
const literals = (...input: string[]) => (
	input.reduce<{ [key: string]: string; }>((acc, lit) => {
		acc[lit] = lit;
		return acc;
	}, {})
);
const baseState = (): any => ({
	ws: /[ \t]+/,
    nl: { match: /[\r]?[\n]/, lineBreaks: true },
    comment: /[;#].*?$/,
    ini_section_header: {
        match: /\[.*?\]/,
        value: (s: string) => s.substring(1, s.length - 1),
    },
    integer: {
        match: /-?[0-9]+/,
        value: (s: string) => Number(s),
    },
    segment: {
        match: /[^=\r\n]+/,
    },
});

const wmcStates: any = {
	main: {
		...baseState(),
		"=": { match: "=", push: "eq" },
	},
	eq: {
		nl: { match: /[\r]?[\n]/, lineBreaks: true },
		value: { match: /[^\r\n]+/, pop: true },
	},
};
const lexer = moo.states(wmcStates);

const normalizeArray = (input: any): any[] => input == null ? [] : (Array.isArray(input) ? input : [input]);

%}

@lexer lexer

File ->
	FileLines {% id %}

FileLines ->
	FileLine {% (d) => [...normalizeArray(d[0])] %}
	| FileLines %nl FileLine  {% (d) => [...normalizeArray(d[0]), ...normalizeArray(d[2])] %}

FileLine ->
	_ TopLevelStmt _ {% (d) => [d[1]] %}
	| _ Comment {% (d) => normalizeArray(d[1]) %}
	| blank_line {% () => [] %}

TopLevelStmt ->
	IniSectionHeader {% id %}
	| Setting {% id %}

Setting ->
	Segment "=" Value {% (d) => ({ type: "setting", name: d[0], eq: d[2] }) %}
	| Segment "=" {% (d) => ({ type: "setting", name: d[0], eq: undefined }) %}

Segment ->
	%segment {% id %}
	| Integer {% id %}

IniSectionHeader ->
	%ini_section_header {% id %}

Value ->
	%value {% id %}

Comment ->
	%comment {% id %}

# Primitives

Integer ->
	%integer {% id %}

# Whitespace

blank_line ->
	_ {% () => null %}

next_line ->
	_ %nl _ {% () => null %}

next_present_line -> # "present" is borrowed from ruby, just means non-whitespace
	_ %nl _n {% () => null %}

multi_line_ws ->
	%ws | %nl {% () => null %}

_ ->
	%ws:* {% () => null %}
__ ->
	%ws:+ {% () => null %}
_n ->
	multi_line_ws:* {% () => null %}
__n ->
	multi_line_ws:+ {% () => null %}


