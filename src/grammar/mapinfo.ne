@preprocessor typescript

@{%

const moo = require('moo');

let lexer = moo.compile({
	ws: /[ \t]+/,
	nl: { match: /[\r]?[\n]/, lineBreaks: true },
	comment: {
		match: /\/\/[^\r\n]*/,
		value: (s: string) => s.substring(2),
	},
	string: {
		match: /"(?:[^\n\\"]|\\["\\ntbfr])*"/,
		value: (s: string) => JSON.parse(s),
	},
	identifier: {
		match: /[a-zA-Z_][a-zA-Z_0-9]*/,
		type: moo.keywords({
			"kw_bool": ["false", "true"],
			"kw_include": "include",
		})
	},
	number: {
		match: /-?[0-9]+(?:\.[0-9]+)?/,
		value: (s: string) => Number(s),
	},
	"{": "{",
	"}": "}",
	"=": "=",
	",": ",",
});

const normalizeArray = (input: any): any[] => input == null ? [] : (Array.isArray(input) ? input : [input]);

%}

@lexer lexer

File ->
	FileLines {% id %}

FileLines ->
	FileLine {% (d) => normalizeArray(d[0]) %}
	| FileLine %nl FileLines {% (d) => [...normalizeArray(d[0]), ...normalizeArray(d[2])] %}

FileLine ->
	_ TopLevelStmt _ (Comment):? {% (d) => [...normalizeArray(d[1]), ...normalizeArray(d?.[3])] %}
	| _ Comment {% (d) => normalizeArray(d[1]) %}
	| _ {% (d) => null %}

TopLevelStmt ->
	IncludeStmt {% id %}
	| Directive {% id %}
	| Block {% id %}

IncludeStmt ->
	%kw_include __ String {% (d) => ({ type: "include", path: d[2] }) %}

BlockType ->
	Identifier {% id %}

BlockHeader ->
	BlockType
	{% (d) => ({ type: d[0] }) %}
	| BlockType __ String
	{% (d) => ({ type: d[0], name: d[2] }) %}
	| BlockType __ String __ String
	{% (d) => ({ type: d[0], name: d[2], name2: d[4] }) %}
	| BlockType __ String __ "lookup" __ String
	{% (d) => ({ type: d[0], name: d[2], lookup: d[6] }) %}
	| BlockType __ Identifier
	{% (d) => ({ type: d[0], identifier: d[2] }) %}
	| BlockType __ Number
	{% (d) => ({ type: d[0], number: d[2] }) %}

Block ->
	BlockHeader BlockStart BlockBody BlockEnd {% (d) => ({ type: "block", header: d[0], body: d[2] }) %}

BlockStart ->
    _ (Comment %nl {% id %} | %nl _n {% (d) => null %}):? "{" {% (d) => d[1] %}

BlockEnd ->
    "}" _ Comment:? {% (d) => d?.[2] %}

BlockBody ->
	BlockLine {% (d) => normalizeArray(d[0]) %}
	| BlockLine %nl BlockBody {% (d) => [...normalizeArray(d[0]), ...normalizeArray(d[2])] %}

BlockLine ->
	_ Block {% (d) => normalizeArray(d[1]) %}
	| _ BlockStmt _ (Comment):? {% (d) => [...normalizeArray(d[1]), ...normalizeArray(d[3])] %}
	| _ Comment {% (d) => normalizeArray(d[1]) %}
	| _ {% (d) => null %}

BlockStmt ->
	Assignment {% id %}
	| Identifier {% id %}

Assignment ->
	Identifier _ "=" _ ValueExpr (_ ";"):? {% (d) => ({ type: "assignment", identifier: d[0], value: d[4] }) %}

Comment ->
	%comment {% id %}

Identifier ->
	%identifier {% id %}

ValueExpr ->
	Array {% id %}
	| Literal {% id %}

Array ->
	Expression (_ "," _ Expression):+ {% (d) => ({ type: "array", values: [d[0], ...d[1].flatMap((x: any) => x[3])] }) %}

Expression ->
	Literal {% id %}

Literal ->
	Identifier {% id %}
	| Number {% id %}
	| String {% id %}
	| Boolean {% id %}

Directive ->
	("clearepisodes" {% id %} | "clearskills" {% id %}) {% (d) => ({ type: "directive", value: d[0].value }) %}

Boolean ->
	%kw_bool {% id %}

String ->
	%string {% id %}

Number ->
	%number {% id %}

# Whitespace

next_line ->
	_ %nl _ {% () => [] %}

next_present_line -> # "present" is borrowed from ruby, just means non-whitespace
	_ %nl _n {% () => [] %}

multi_line_ws ->
	%ws | %nl {% () => [] %}

_ ->
	%ws:* {% () => [] %}
__ ->
	%ws:+ {% () => [] %}
_n ->
	multi_line_ws:* {% () => [] %}
__n ->
	multi_line_ws:+ {% () => [] %}
