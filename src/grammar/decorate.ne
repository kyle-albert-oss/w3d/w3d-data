@preprocessor typescript

@{%

const moo = require('moo');
const literals = (...input: string[]) => (
	input.reduce<{ [key: string]: string; }>((acc, lit) => {
		acc[lit] = lit;
		return acc;
	}, {})
);
const debugBlanks = false;
const blank = (str: string) => () => debugBlanks ? [`BLANK-${str}`] : [];

const baseState = (): any => ({
	ws: /[ \t]+/,
	nl: { match: /[\r]?[\n]/, lineBreaks: true },
	ml_comment: {
		match: /\/\*[^]*?\*\//,
	},
	comment: {
		match: /\/\/.*?$/, // TODO: need states to manage nested multiline comment parsing... (is it worth it? *no*)
		value: (s: string) => s.substring(2),
	},
	string: {
		match: /"(?:[^\n\\"]|\\["\\ntbfr])*"/,
		value: (s: string) => JSON.parse(s),
	},
	reference: {
		match: /\$[a-zA-Z][a-zA-Z_0-9]*/,
		value: (s: string) => s.substring(1),
	},
	sprite_frames: /(?:[A-Z]*)(?:[\[\]\\]+[A-Z]*)+/,
	states: "states", // HACK: stop this from being interpreted as an identifier
	flag: {
        match: /[+-][A-Z\.]+/,
        value: (s: string) => ({ type: "flag", modifier: s.charAt(0), pathStr: s.substring(1), path: s.substring(1).split(/\./), }),
    },
    action_identifier: /(?:A_|Exit_|Teleport_)[a-zA-Z][a-zA-Z_0-9]*/, // TODO: don't think this is gonna work
	identifier: {
		match: /[a-zA-Z_][a-zA-Z_0-9]*/,
		type: moo.keywords({
			"kw_bool": ["false", "true"],
			"kw_control_flow": ["loop", "wait", "goto", "stop"],
			"kw_native": "native",
		})
	},
	hex: {
		match: /0x[0-9A-Fa-f]+/,
		value: (s: string) => parseInt(s, 16),
	},
	number: {
		match: /-?(?:\d+(?:\.\d+)?|\.\d+)/,
		value: (s: string) => Number(s),
	},
	operator: /[+&\-\*\/\|]/,
	...literals(":", "{", "}", "[", "]", "=", ",", ";", "."),
});

const decorateStates: any = {
	main: {
		...baseState(),
		"(": { match: "(", push: "parenthesis" },
		")": { match: ")", pop: true },
	},
	parenthesis: {
		...baseState(),
		...literals("(", ")"),
	}
};
delete decorateStates.parenthesis.sprite_frames;

const lexer = moo.states(decorateStates);

const spriteFrameIdentifierRejectExp = /[a-z_0-9]/;

const normalizeArray = (input: any): any[] => input == null ? [] : (Array.isArray(input) ? input : [input]);

%}

@lexer lexer

File ->
	FileLines {% id %}

FileLines ->
	FileLine {% (d) => [...normalizeArray(d[0])] %}
	| FileLines %nl FileLine  {% (d) => [...normalizeArray(d[0]), ...normalizeArray(d[2])] %}

FileLine ->
	_ TopLevelStmt _ (Comment):? {% (d) => [d[1], ...normalizeArray(d?.[3])] %}
	| _ Comment {% (d) => normalizeArray(d[1]) %}
	| blank_line {% blank("FileLine") %}

TopLevelStmt ->
	IncludeStmt {% id %}
	| ActorBlock {% id %}

ActorBlock ->
	ActorBlockHeader BlockStart ActorBlockLines BlockEnd {% (d) => ({ type: "block", header: d[0], body: d[2] }) %}

ActorBlockHeader ->
	"actor" __ Identifier
	(__ ":" __ Identifier {% (d) => ({ modifier: "extends", value: d[3] }) %}):?
	(__ "replaces" __ Identifier {% (d) => ({ modifier: "replaces", value: d[3] }) %}):?
	(__ Number {% (d) => ({ modifier: "tileNum", value: d[1] }) %}):?
	(__ "native" {% (d) => ({ modifier: "native" }) %}):?
	{% (d) => ({
		extends: d.find((n: any) => n?.modifier === "extends")?.value,
		name: d[2],
		native: !!d.find((n: any) => n?.modifier === "native"),
		replaces: d.find((n: any) => n?.modifier === "replaces")?.value,
		tileNum: d.find((n: any) => n?.modifier === "tileNum")?.value,
		type: d[0],
	}) %}

ActorBlockLines ->
	ActorBlockLine {% (d) => normalizeArray(d[0]) %}
	| ActorBlockLines %nl ActorBlockLine {% (d) => [...normalizeArray(d[0]), ...normalizeArray(d[2])] %}

ActorBlockLine ->
	_ ActorStatesBlock _ (Comment):? {% (d) => [d[1], ...normalizeArray(d?.[3])] %}
	| _ ActorBlockStmt _ (Comment):? {% (d) => [d[1], ...normalizeArray(d?.[3])] %}
	| _ Comment {% (d) => normalizeArray(d[1]) %}
	| blank_line {% blank("ActorBlockLine") %}

ActorBlockStmt ->
	Property {% id %}
	| Flag {% id %}
	| "action" %kw_native

#
# BEGIN Actor States Block
#

ActorStatesBlock ->
	%states BlockStart ActorStatesBlockLines BlockEnd {% (d) => ({ type: "block", header: d[0], body: d[2] }) %}

ActorStatesBlockLines ->
	ActorStatesBlockLine {% (d) => normalizeArray(d[0]) %}
	| ActorStatesBlockLines %nl ActorStatesBlockLine {% (d) => [...normalizeArray(d[0]), ...normalizeArray(d[2])] %}

ActorStatesBlockLine ->
	_ ActorStateSeqStmt _ (Comment):? {% (d) => [d[1], ...normalizeArray(d?.[3])] %}
	| _ Comment {% (d) => normalizeArray(d[1]) %}
	| blank_line {% blank("ActorStatesBlockLine") %}

ActorStateSeqStmt ->
	ActorStateSeqLabel {% id %}
	| ActorStateSeqControlFlow {% id %}
	| ActorStateDef {% id %}

ActorStateSeqLabel ->
	ActorStateLabel ":" {% (d) => ({ ...d[0], type: "state-label" }) %}

ActorStateSeqControlFlow ->
	"fail" {% (d) => ({ type: "control-flow", keyword: d[0] }) %}
	| "goto" __ ActorStateLabel ("+" _ Number {% (d) => d[2] %}):? {% (d) => ({ type: "control-flow", keyword: d[0], nextState: d[2], nextStateOffset: d?.[3] ?? undefined }) %}
	| "loop" {% (d) => ({ type: "control-flow", keyword: d[0] }) %}
	| "stop" {% (d) => ({ type: "control-flow", keyword: d[0] }) %}
	| "wait" {% (d) => ({ type: "control-flow", keyword: d[0] }) %}

ActorStateLabel ->
	Identifier {% id %}

#
# END Actor States Block
#

#
# BEGIN Actor State Def Line
#

ActorStateDef ->
	Identifier
	__ ActorStateDefSpriteFrames
	__ ActorStateDefDuration
	(__ ActorStateDefKeyword {% (d) => ({ type: "actor-keyword", value: d[1] }) %}):?
	((
		__ ActorActionFn {% (d) => ({ type: "tmp", value: [d[1]] }) %}
		| __ ActorActionFn __ ActorActionFn {% (d) => ({ type: "tmp", value: [d[1], d[3]] }) %}
	):? {% id %})
	{% (d) => ({
		type: "state-def",
		actionFns: d.find((n: any) => n?.type === "tmp")?.value,
		duration: d[4],
		frames: d[2],
		keyword: d.find((n: any) => n?.type === "actor-keyword")?.value,
		name: d[0],
	}) %}

ActorStateDefSpriteFrames ->
	%sprite_frames {% id %}
	| %identifier {% (d, l, rj) => (d[0].value.match(spriteFrameIdentifierRejectExp) ? rj : d[0]) %}

ActorStateDefDuration ->
	Number {% (d) => ({ type: "tics", value: d[0] }) %}
	| "random" "(" _ Number _ "," _ Number _ ")" {% (d) => ({ type: "tics-random", min: d[3], max: d[7] }) %}

ActorStateDefKeyword ->
	Identifier {% id %}

ActorActionFn ->
	%action_identifier {% (d) => ({ type: "action-fn", identifier: d[0], args: [] }) %}
	| %action_identifier "(" ArgList ")" {% (d) => ({ type: "action-fn", identifier: d[0], args: d[2] ?? [] }) %}

ActorThinker -> # TODO: possible values?
	Identifier {% id %}

#
# END Actor State Def Line
#

Property ->
	PropertyName __ ExprList {% (d) => ({ type: "property", name: d[0], valueExpr: d[2] }) %}
	| PropertyName {% id %}

Flag ->
	%flag {% id %}

PropertyName ->
	Identifier ("." Identifier {% (d) => d[1] %}):*
	{% (d) => {
		const path = [d[0], ...normalizeArray(d[1])];
		return {
			type: "property-name",
			path,
			pathStr: path.map((p) => p.value).join("."),
		};
	} %}

IncludeStmt ->
	"#include" __ String {% (d) => ({ type: "include", path: d[2] }) %}

ArgList ->
	null {% () => [] %}
	| _ Expr _ {% (d) => normalizeArray(d[1]) %}
	| _ Expr _ "," ArgList {% (d) => [...normalizeArray(d[1]), ...normalizeArray(d[4])] %}

ExprList ->
	Expr {% (d) => d[0] %}
	| Expr _ "," _ ExprList {% (d) => ({ type: "expr-list", values: [...normalizeArray(d[0]), ...normalizeArray(d[4])] }) %}

FnCallExp ->
	Identifier "(" ExprList ")" {% (d) => ({ type: "fn-call", identifier: d[0], args: d[3] }) %}
	| Identifier "[" Identifier "]" "(" ExprList ")" {% (d) => ({ type: "fn-call", identifier: d[0], accessor: d[2], args: d[5] }) %}

Expr ->
	AdditiveExpr {% id %}

AdditiveExpr ->
	MultiplicativeExpr {% id %}
	| MultiplicativeExpr _ [+-] _ AdditiveExpr {% (d) => ({ type: "binary-op", left: d[0], operator: d[2], right: d[4] }) %}

# TODO: does & and | belong here? need to look up precedence rules. Probably the same as C++ but just sticking them here for now.
MultiplicativeExpr ->
	UnaryExp {% id %}
	| UnaryExp _ [*/&|] _ MultiplicativeExpr {% (d) => ({ type: "binary-op", left: d[0], operator: d[2], right: d[4] }) %}

# TODO: /* */ multiline comments
Comment ->
	%comment {% id %}
	| %ml_comment {% id %}

Identifier ->
	%identifier {% id %}

BitwiseOr ->
	Identifier (_ "|" _ Identifier {% (d) => d[3] %}):+ {% (d) => ({ type: "bitwiseOr", values: [d[0], ...d[1]] }) %}

UnaryExp ->
	Literal {% id %}
	| FnCallExp {% id %}
	| "(" Expr ")" {% (d) => d[1] %}

Literal ->
	Identifier {% id %}
	| Number {% id %}
	| String {% id %}
	| Boolean {% id %}
	| Reference {% id %}

BlockStart ->
	_ (Comment %nl {% id %} | %nl _n {% (d) => null %}):? "{" {% (d) => d[1] %}

BlockEnd ->
	"}" _ Comment:? {% (d) => d[2] %}

Operator ->
	%operator {% id %}

Reference ->
	%reference {% id %}

Boolean ->
	%kw_bool {% id %}

String ->
	%string {% id %}

Number ->
	%number {% id %}
	| %hex {% id %}

# Whitespace

blank_line ->
	_ {% () => null %}

next_line ->
	_ %nl _ {% () => null %}

next_present_line -> # "present" is borrowed from ruby, just means non-whitespace
	_ %nl _n {% () => null %}

multi_line_ws ->
	%ws | %nl {% () => null %}

_ ->
	%ws:* {% () => null %}
__ ->
	%ws:+ {% () => null %}
_n ->
	multi_line_ws:* {% () => null %}
__n ->
	multi_line_ws:+ {% () => null %}
