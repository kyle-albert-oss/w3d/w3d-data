@preprocessor typescript

# TODO list (WIP)
# See https://www.doomworld.com/eternity/engine/stuff/udmf11.txt
# find examples or write a sample file
# add lz/ec wolf additions http://maniacsvault.net/ecwolf/wiki/Universal_Wolfenstein_Map_Format

@{%

const moo = require('moo');
const literals = (...input: string[]) => (
	input.reduce<{ [key: string]: string; }>((acc, lit) => {
		acc[lit] = lit;
		return acc;
	}, {})
);
const debugBlanks = false;
const blank = (str: string) => () => debugBlanks ? [`BLANK-${str}`] : [];

const lexer = moo.compile({
	ws: /[ \t]+/,
	nl: { match: /[\r]?[\n]/, lineBreaks: true },
	ml_comment: {
		match: /\/\*[^]*?\*\//,
	},
	comment: {
		match: /\/\/.*?$/,
		value: (s: string) => s.substring(2),
	},
	string: {
		match: /"(?:[^\n\\"]|\\["\\ntbfr])*"/,
		value: (s: string) => JSON.parse(s),
	},
	identifier: {
		match: /[a-zA-Z_][a-zA-Z_0-9]*/,
		type: moo.keywords({
			"kw_bool": ["false", "true"],
		})
	},
	hex: {
		match: /0x[0-9A-Fa-f]+/,
		value: (s: string) => parseInt(s, 16),
	},
	float: {
		match: /[+-]?\d+\.\d*(?:[eE][+-]?\d+)?/,
		value: (s: string) => parseFloat(s),
	},
	integer: {
		match: /-?\d+/,
		value: (s: string) => Number(s),
	}.
	operator: /[+&\-\*\/\|]/,
	...literals("{", "}", "(", ")" "=", ";"),
});

const normalizeArray = (input: any): any[] => input == null ? [] : (Array.isArray(input) ? input : [input]);

%}

@lexer lexer

File ->
	FileLines {% id %}

FileLines ->
	FileLine {% (d) => [...normalizeArray(d[0])] %}
	| FileLines %nl FileLine  {% (d) => [...normalizeArray(d[0]), ...normalizeArray(d[2])] %}

FileLine ->
	_ GlobalExpr _ (Comment):? {% (d) => [d[1], ...normalizeArray(d?.[3])] %}
	| _ Comment {% (d) => normalizeArray(d[1]) %}
	| blank_line {% blank("FileLine") %}

GlobalExpr ->
	Block {% id %}
	| AssignmentExpr {% id %}

Block ->
	Identifier BlockStart BlockLines BlockEnd {% (d) => ({ type: "block", header: d[0], body: d[2] }) %}

BlockLines ->
	BlockLine {% (d) => normalizeArray(d[0]) %}
	| BlockLines %nl BlockLine {% (d) => [...normalizeArray(d[0]), ...normalizeArray(d[2])] %}

BlockLine ->
	_ BlockStmt _ (Comment):? {% (d) => [d[1], ...normalizeArray(d?.[3])] %}
	| _ Comment {% (d) => normalizeArray(d[1]) %}
	| blank_line {% () => [] %}

BlockStmt ->
	AssignmentExpr {% id %}

BlockStart ->
	_ (Comment %nl {% id %} | %nl _n {% (d) => null %}):? "{" {% (d) => d[1] %}

BlockEnd ->
	"}" _ Comment:? {% (d) => d[2] %}

AssignmentStmt ->
	%identifier _ "=" _ ValueExpr _ ";" {% (d) => ({ type: "assignment-stmt", identifier: d[0], valueExpr: d[4] }) %}

ValueExpr ->
	String {% id %}
	| Number {% id %}
	| Boolean {% id %}

Boolean ->
	%kw_bool {% id %}

String ->
	%string {% id %}

Number ->
	%number {% id %}
	| %float {% id %}
	| %hex {% id %}

Identifier ->
	%identifier {% id %}

# Whitespace

blank_line ->
	_ {% () => null %}

next_line ->
	_ %nl _ {% () => null %}

next_present_line -> # "present" is borrowed from ruby, just means non-whitespace
	_ %nl _n {% () => null %}

multi_line_ws ->
	%ws | %nl {% () => null %}

_ ->
	%ws:* {% () => null %}
__ ->
	%ws:+ {% () => null %}
_n ->
	multi_line_ws:* {% () => null %}
__n ->
	multi_line_ws:+ {% () => null %}
