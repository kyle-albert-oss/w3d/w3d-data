@preprocessor typescript

@{%

const moo = require('moo');

let lexer = moo.compile({
	ws: /[ \t]+/,
	nl: { match: /[\r]?[\n]/, lineBreaks: true },
	comment: {
		match: /\/\/[^\r\n]*/,
		value: (s: string) => s.substring(2),
	},
	string: {
		match: /"(?:[^\n\\"]|\\["\\ntbfr])*"/,
		value: (s: string) => JSON.parse(s),
	},
	reference: {
		match: /\$[a-zA-Z][a-zA-Z_0-9]*/,
		value: (s: string) => s.substring(1),
	},
	identifier: {
		match: /[a-zA-Z_][a-zA-Z_0-9]*/,
		type: moo.keywords({
			"fl_patrol": ["HOLOWALL", "PATHING"],
			"kw_bool": ["false", "true"],
		})
	},
	number: {
		match: /[0-9]+(?:\.[0-9]+)?/,
		value: (s: string) => Number(s),
	},
	"{": "{",
	"}": "}",
	"=": "=",
	",": ",",
	";": ";",
	"|": "|",
});

%}

@lexer lexer

File ->
	FileLines {% id %}

FileLines ->
	FileLine {% (d) => [d[0]] %}
	| FileLine %nl FileLines {% (d) => [d[0], ...d[2]] %}
	# below 2 sub-rules handle blank lines
	| _ %nl FileLines {% (d) => d[2] %}
	| _ {% (d) => [] %}

FileLine ->
	_ TopLevelStmt _ (Comment):? {% (d) => (d[1]) %}
	| _ Comment {% (d) => ([]) %}

TopLevelStmt ->
	IncludeStmt {% id %}
	| FeatureStmt {% id %}
	| Block {% id %}

Block ->
	BlockHeader __n "{" BlockBody "}" {% (d) => ({ type: "block", header: d[0], body: d[3] }) %}

BlockHeader ->
	BlockType
	{% (d) => ({ type: d[0] }) %}
	| BlockType (__ (String|Identifier|Number)):+
	{% (d) => ({ type: d[0], modifiers: d[1].flatMap((x: any) => x[1]) }) %}

# TODO: generalize to identifier? wouldn't require changes but might make grammar more ambiguous
BlockType ->
	"ceiling" {% id %}
	| "floor" {% id %}
	| "modzone" {% id %}
	| "music" {% id %}
	| "things" {% id %}
	| "tile" {% id %}
	| "tiles" {% id %}
	| "trigger" {% id %}
	| "zone" {% id %}

BlockBody ->
	BlockLine {% (d) => [d[0]] %}
	| BlockLine %nl BlockBody {% (d) => [d[0], ...d[2]] %}
	# below 2 sub-rules handle blank lines
	| _ %nl BlockBody {% (d) => (d[2]) %}
	| _ {% (d) => [] %}

BlockLine ->
	_ Block _ (Comment):? {% (d) => (d[1]) %}
	| _ BlockStmt _ (Comment):? {% (d) => (d[1]) %}
	| _ Comment {% (d) => ([]) %}

BlockStmt ->
	Assignment {% id %}
	| Tuple {% id %}
	| Directive {% id %}

Assignment ->
	Identifier _ "=" _ ValueExpr (_ ";"):? {% (d) => ({ type: "assignment", identifier: d[0], value: d[4] }) %}

Directive ->
	Identifier (__ (Number | Identifier | String)):+ _ ";" {% (d) => ({ type: "directive", parameters: d.slice(2, d.length - 2).filter((d, i) => i % 2 === 0) }) %}

Tuple ->
	"{" Array "}" {% (d) => ({ type: "tuple", values: d[1].values }) %}

BitwiseOr ->
	Flag (_ "|" _ Flag):+ {% (d) => ({ type: "bitwiseOr", values: [d[0], ...d[1].flatMap((x: any) => x[3])] }) %}

IncludeStmt ->
	"include" __ String {% (d) => ({ type: "include", path: d[2] }) %}

FeatureStmt ->
	"enable" __ Identifier ";" {% (d) => ({ type: "feature", on: true, feature: d[2] }) %}

Comment ->
	%comment {% id %}

Identifier ->
	%identifier {% id %}

ValueExpr ->
	Array {% id %}
	| Literal {% id %}

Array ->
	Expression (_ "," _ Expression):+ {% (d) => ({ type: "array", values: [d[0], ...d[1].flatMap((x: any) => x[3])] }) %}

Expression ->
	BitwiseOr {% id %}
	| Literal {% id %}

Literal ->
	Identifier {% id %}
	| Number {% id %}
	| String {% id %}
	| Boolean {% id %}
	| Reference {% id %}
	| Flag {% id %}

Flag ->
	%fl_patrol {% id %}

Reference ->
	%reference {% id %}

Boolean ->
	%kw_bool {% id %}

String ->
	%string {% id %}

Number ->
	%number {% id %}

# Whitespace

next_line ->
	_ %nl _ {% () => [] %}

next_present_line -> # "present" is borrowed from ruby, just means non-whitespace
	_ %nl _n {% () => [] %}

multi_line_ws ->
	%ws | %nl {% () => [] %}

_ ->
	%ws:* {% () => [] %}
__ ->
	%ws:+ {% () => [] %}
_n ->
	multi_line_ws:* {% () => [] %}
__n ->
	multi_line_ws:+ {% () => [] %}
