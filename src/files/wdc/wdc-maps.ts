import * as fs from "fs-extra";

import { WolfFileReader } from "../shared/WolfFileReader";
import { IGameMap, IGameMapPlane } from "../../models/map";
import { WolfBuffer } from "../../buffer/WolfBuffer";
import { WolfFileWriter } from "../shared/WolfFileWriter";
import { DeepReadonly } from "ts-essentials";
import tsDefaults, { ObjectDefaults } from "ts-defaults";
import { fixedLengthArray } from "../../utils/collections";

// TODO: eliminate duped code

export const readWdcMapFile = async (filePath: string): Promise<IGameMap[]> => {
	const result: IGameMap[] = [];
	const wfr = new WolfFileReader(filePath);

	let buf = WolfBuffer.alloc(6);
	const wdcVersion = await wfr.readStringWithBuffer(buf, 6, "ascii");
	if (wdcVersion !== "WDC3.1") {
		throw new Error(`${wdcVersion} file version is not supported. Use a WDC 3.1 formatted map file.`);
	}
	const numMaps = await wfr.readDWordWithBuffer(buf);
	const [numPlanes, maxMapNameLength] = await wfr.readWordsWithBuffer(buf, 2);

	const mapHeaderNumBytes = maxMapNameLength + 2 * WolfBuffer.WORD_SEGMENT_LENGTH;
	buf = WolfBuffer.alloc(mapHeaderNumBytes);

	for (let mapIndex = 0; mapIndex < numMaps; mapIndex++) {
		buf.position = 0;
		await wfr.readBytesIntoBuffer(buf, mapHeaderNumBytes);
		const mapName = buf.readString(maxMapNameLength, { encoding: "ascii" });
		const [mapWidth, mapHeight] = buf.readWords(2);
		const mapDataNumBytes = numPlanes * mapWidth * mapHeight * WolfBuffer.WORD_SEGMENT_LENGTH;

		const mapBuf = WolfBuffer.alloc(mapDataNumBytes);
		await wfr.readBytesIntoBuffer(mapBuf, mapDataNumBytes);

		const planes: IGameMapPlane[] = [];
		for (let p = 0; p < numPlanes; p++) {
			const data: number[][] = [];
			for (let y = 0; y < mapHeight; y++) {
				data.push(mapBuf.readWords(mapWidth));
			}

			planes.push({
				data,
				height: mapHeight,
				index: p,
				width: mapWidth,
			});
		}

		result.push({
			height: mapHeight,
			index: mapIndex,
			name: mapName,
			planes,
			width: mapWidth,
		});
	}

	await wfr.close();

	return result;
};

export const readWdcMapFileFromBuffer = (buffer: WolfBuffer): IGameMap[] => {
	const result: IGameMap[] = [];

	const wdcVersion = buffer.readString(6, { encoding: "ascii" });
	if (wdcVersion !== "WDC3.1") {
		throw new Error(`${wdcVersion} file version is not supported. Use a WDC 3.1 formatted map file.`);
	}
	const numMaps = buffer.readDWord();
	const [numPlanes, maxMapNameLength] = buffer.readWords(2);

	for (let mapIndex = 0; mapIndex < numMaps; mapIndex++) {
		const mapName = buffer.readString(maxMapNameLength, { encoding: "ascii" });
		const [mapWidth, mapHeight] = buffer.readWords(2);

		const planes: IGameMapPlane[] = [];
		for (let p = 0; p < numPlanes; p++) {
			const data: number[][] = [];
			for (let y = 0; y < mapHeight; y++) {
				data.push(buffer.readWords(mapWidth));
			}

			planes.push({
				data,
				height: mapHeight,
				index: p,
				width: mapWidth,
			});
		}

		result.push({
			height: mapHeight,
			index: mapIndex,
			name: mapName,
			planes,
			width: mapWidth,
		});
	}

	return result;
};

export interface IWriteWdcMapOpts {
	test?: string;
	numPlanes?: number;
	maxMapNameBytes?: number;
}

export const DEFAULT_WRITE_WDC_MAP_OPTS: DeepReadonly<ObjectDefaults<IWriteWdcMapOpts, "numPlanes" | "maxMapNameBytes">> = {
	numPlanes: 3,
	maxMapNameBytes: 16,
};

export const writeWdcMapFile = async (filePath: string, maps: IGameMap[], opts?: IWriteWdcMapOpts): Promise<string> => {
	try {
		const options = tsDefaults(opts ?? ({} as IWriteWdcMapOpts), DEFAULT_WRITE_WDC_MAP_OPTS, ["numPlanes", "maxMapNameBytes"]);
		const { numPlanes, maxMapNameBytes } = options;

		const wfw = new WolfFileWriter(filePath);

		// write header
		const headerBuf = WolfBuffer.allocUnsafe(14);
		headerBuf.writeString("WDC3.1", { encoding: "ascii" });
		headerBuf.writeDWord(maps.length);
		headerBuf.writeWords([numPlanes, maxMapNameBytes]);
		await wfw.writeFromBuffer(headerBuf, 14);

		const mapHeaderBuf = WolfBuffer.allocUnsafe(maxMapNameBytes + 2 * WolfBuffer.WORD_SEGMENT_LENGTH);
		for (let mapIndex = 0; mapIndex < maps.length; mapIndex++) {
			const map = maps[mapIndex];
			const { height, name, planes, width } = map;
			if (planes.length > numPlanes) {
				throw new Error(`Map ${mapIndex} has ${planes.length} planes but the max number of planes is ${numPlanes}.`);
			}

			// write map header
			mapHeaderBuf.position = 0;
			mapHeaderBuf.writeString(name, { fixedByteLength: maxMapNameBytes, encoding: "ascii" });
			mapHeaderBuf.writeWords([width, height]);
			await wfw.writeBytesFromBuffer(mapHeaderBuf, mapHeaderBuf.buffer.length);

			const mapBuf = WolfBuffer.allocUnsafe(width * height * WolfBuffer.WORD_SEGMENT_LENGTH);

			// write map plane data
			for (let p = 0; p < numPlanes; p++) {
				mapBuf.position = 0;
				let plane = map.planes?.[p];
				if (plane == null) {
					mapBuf.buffer.fill(0);
				} else if (plane.width > width || plane.height > height) {
					throw new Error(
						`Map ${mapIndex} Plane ${p} has width ${plane.width} and height ${plane.height} but max width is ${width} and max height is ${height}.`
					);
				} else {
					for (let y = 0; y < height; y++) {
						mapBuf.writeWords(fixedLengthArray(plane.data[y], width, 0));
					}
				}
				await wfw.writeBytesFromBuffer(mapBuf, mapBuf.buffer.length);
			}
		}

		return filePath;
	} catch (e) {
		fs.unlinkSync(filePath);
		throw e;
	}
};

export const writeWdcMapFileToBuffer = (maps: IGameMap[], opts?: IWriteWdcMapOpts): WolfBuffer => {
	const bufs: WolfBuffer[] = [];
	const options = tsDefaults(opts ?? ({} as IWriteWdcMapOpts), DEFAULT_WRITE_WDC_MAP_OPTS, ["numPlanes", "maxMapNameBytes"]);
	const { numPlanes, maxMapNameBytes } = options;

	// write header
	const headerBuf = WolfBuffer.allocUnsafe(14);
	headerBuf.writeString("WDC3.1", { encoding: "ascii" });
	headerBuf.writeDWord(maps.length);
	headerBuf.writeWords([numPlanes, maxMapNameBytes]);
	bufs.push(headerBuf);

	const mapHeaderBuf = WolfBuffer.allocUnsafe(maxMapNameBytes + 2 * WolfBuffer.WORD_SEGMENT_LENGTH);
	for (let mapIndex = 0; mapIndex < maps.length; mapIndex++) {
		const map = maps[mapIndex];
		const { height, name, planes, width } = map;
		if (planes.length > numPlanes) {
			throw new Error(`Map ${mapIndex} has ${planes.length} planes but the max number of planes is ${numPlanes}.`);
		}

		// write map header
		mapHeaderBuf.position = 0;
		mapHeaderBuf.writeString(name, { fixedByteLength: maxMapNameBytes, encoding: "ascii" });
		mapHeaderBuf.writeWords([width, height]);
		bufs.push(mapHeaderBuf);

		// write map plane data
		for (let p = 0; p < numPlanes; p++) {
			const mapBuf = WolfBuffer.allocUnsafe(width * height * WolfBuffer.WORD_SEGMENT_LENGTH);
			let plane = map.planes?.[p];
			if (plane == null) {
				mapBuf.buffer.fill(0);
			} else if (plane.width > width || plane.height > height) {
				throw new Error(
					`Map ${mapIndex} Plane ${p} has width ${plane.width} and height ${plane.height} but max width is ${width} and max height is ${height}.`
				);
			} else {
				for (let y = 0; y < height; y++) {
					mapBuf.writeWords(fixedLengthArray(plane.data[y], width, 0));
				}
			}
			bufs.push(mapBuf);
		}
	}

	return WolfBuffer.concat(bufs);
};
