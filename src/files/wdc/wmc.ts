import nearley from "nearley";
import * as fs from "fs-extra";
import GRAMMAR_WDC_WMC from "../../grammar/parsers/wdc-wmc-parser";

export const newWdcWmcParser = () => new nearley.Parser(nearley.Grammar.fromCompiled(GRAMMAR_WDC_WMC));

export const readWdcWmcFile = async (filePath: string | Buffer | number, parser = newWdcWmcParser()) =>
	parser.feed(await fs.readFile(filePath, "utf8")).results;
