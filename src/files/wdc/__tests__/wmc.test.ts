import * as path from "path";
import { readWdcWmcFile } from "../wmc";

it("Correctly reads wl6.wmc", async () => {
	const wdcWmcFilePath = path.resolve(process.cwd(), "assets/wmc/wl6.wmc");
	let nodes = await readWdcWmcFile(wdcWmcFilePath);
	expect(nodes.length).toEqual(1); // > 1 = ambiguous

	nodes = nodes[0];

	expect(nodes.length).toEqual(593);

	expect(nodes[0].type).toEqual("ini_section_header");
	expect(nodes[0].value).toEqual("Walls");

	expect(nodes[1].type).toEqual("setting");
	expect(nodes[1].eq.value).toEqual("W,0,8888888888888888888888888888888888888888888888888,Grey Brick 1");

	expect(nodes[57].type).toEqual("setting");
	expect(nodes[57].eq.value).toEqual("L1,104,00000000000000EEEEEEEEEEEEEEEEEEEEE00000000000000,Door (Gold Key - N/S)");

	expect(nodes[374].type).toEqual("ini_section_header");
	expect(nodes[374].value).toEqual("Music Chunk Names");

	expect(nodes[375].type).toEqual("setting");
	expect(nodes[375].eq.value).toEqual("CORNER_MUS (Read This, E1/4M10)");

	expect(nodes[592].type).toEqual("setting");
	expect("eq" in nodes[592]).toBeTruthy();
	expect(nodes[592].eq).toBeUndefined();
});

it("Correctly reads sod.wmc", async () => {
	const wdcWmcFilePath = path.resolve(process.cwd(), "assets/wmc/sod.wmc");
	let nodes = await readWdcWmcFile(wdcWmcFilePath);
	expect(nodes.length).toEqual(1); // > 1 = ambiguous

	nodes = nodes[0];

	expect(nodes.length).toEqual(570);

	expect(nodes[0].type).toEqual("ini_section_header");
	expect(nodes[0].value).toEqual("Walls");

	expect(nodes[1].type).toEqual("setting");
	expect(nodes[1].eq.value).toEqual("W,0,8888888888888888888888888888888888888888888888888,Grey Brick 1");

	expect(nodes[68].type).toEqual("setting");
	expect(nodes[68].eq.value).toEqual("L1,132,00000000000000EEEEEEEEEEEEEEEEEEEEE00000000000000,Door (Gold Key - N/S)");

	expect(nodes[73].type).toEqual("setting");
	expect(nodes[73].eq.value).toEqual("A,D00000D0D000D000D0D00000D00000D0D000D000D0D00000D,Deaf Guard");

	expect(nodes[376].type).toEqual("setting");
	expect(nodes[376].eq.value).toEqual("XFUNKIE_MUS (M2/20)");

	expect(nodes[569].type).toEqual("setting");
	expect(nodes[569].eq.value).toEqual("0,0,0,65,700");
});
