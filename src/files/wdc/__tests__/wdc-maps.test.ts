import * as path from "path";

import * as fs from "fs-extra";

import { readWdcMapFile, readWdcMapFileFromBuffer, writeWdcMapFileToBuffer, writeWdcMapFile } from "../wdc-maps";
import { WolfBuffer } from "../../../buffer/WolfBuffer";
import { IGameMap } from "../../../models/map";
import { isCIEnv } from "../../../utils/tests";

const testWolf1Map1 = (map: IGameMap) => {
	expect(map.index).toEqual(0);
	expect(map.width).toEqual(64);
	expect(map.height).toEqual(64);
	expect(map.name).toEqual("Wolf1 Map1");

	const planes = map.planes;
	expect(planes.length).toEqual(3);

	let p = planes[0];
	let { data: d } = p;

	expect(d[63][63]).toEqual(1);
	expect(d[0][0]).toEqual(1);
	expect(d[21][13]).toEqual(6);
	expect(d[38][34]).toEqual(91);
	expect(d[42][52]).toEqual(118);
	expect(d[22][30]).toEqual(10);
	expect(d[63][63]).toEqual(1);

	p = planes[1];
	d = p.data;

	expect(d[0][0]).toEqual(0);
	expect(d[19][10]).toEqual(0);
	expect(d[44][52]).toEqual(97);
	expect(d[33][29]).toEqual(27);
	expect(d[57][31]).toEqual(124);
};

describe("WDC map file read", () => {
	it("Reads a single map from a map file.", async () => {
		const maps = await readWdcMapFile(path.resolve(process.cwd(), "assets/maps/wolf1map1.map"));

		expect(maps.length).toEqual(1);

		testWolf1Map1(maps[0]);
	});

	it("Reads a single map from a buffer.", () => {
		const buf = WolfBuffer.allocUnsafe(6 + 4 + 4 + 16 + 4 + 3 * (64 * 64 * 2));
		buf.writeString("WDC3.1", { encoding: "ascii" });
		buf.writeDWord(1);
		buf.writeWords([3, 16]);
		buf.writeString("FirstMap", { fixedByteLength: 16, encoding: "ascii" });
		buf.writeWords([64, 64]);
		for (let p = 0; p < 3; p++) {
			buf.writeWords(Array(64 * 64).fill(p + 1));
		}
		buf.position = 0;

		const maps = readWdcMapFileFromBuffer(buf);

		expect(maps.length).toEqual(1);

		const map = maps[0];

		expect(map.name).toEqual("FirstMap");
		expect(map.width).toEqual(64);
		expect(map.height).toEqual(64);
		expect(map.planes.length).toEqual(3);

		let uniqPlaneValues = [...new Set(map.planes[0].data.flatMap((d) => d))];
		expect(uniqPlaneValues.length).toEqual(1);
		expect(uniqPlaneValues[0]).toEqual(1);

		uniqPlaneValues = [...new Set(map.planes[1].data.flatMap((d) => d))];
		expect(uniqPlaneValues.length).toEqual(1);
		expect(uniqPlaneValues[0]).toEqual(2);

		uniqPlaneValues = [...new Set(map.planes[2].data.flatMap((d) => d))];
		expect(uniqPlaneValues.length).toEqual(1);
		expect(uniqPlaneValues[0]).toEqual(3);
	});
});

describe("Write WDC map file", () => {
	let maps: IGameMap[];

	beforeAll(async () => {
		maps = await readWdcMapFile(path.resolve(process.cwd(), "assets/maps/wolf1map1.map"));
	});

	const getPlaneXYFromBuffer = (buf: WolfBuffer, plane: number, x: number, y: number, width: number, height: number, offset: number) =>
		buf.readWord(
			offset +
				plane * WolfBuffer.WORD_SEGMENT_LENGTH * height * width +
				WolfBuffer.WORD_SEGMENT_LENGTH * y * height +
				WolfBuffer.WORD_SEGMENT_LENGTH * x
		);

	it("Writes WDC map to buffer", () => {
		const buf = writeWdcMapFileToBuffer(maps, { maxMapNameBytes: 16, numPlanes: 3 });
		const { buffer } = buf;
		const planeDataOffset = 6 + 4 + 4 + 16 + 4;
		expect(buffer.length).toEqual(planeDataOffset + 3 * (64 * 64 * 2));

		expect(buf.readString(6, { encoding: "ascii" })).toEqual("WDC3.1");
		expect(buf.readDWord()).toEqual(1);
		expect(buf.readWords(2)).toEqual([3, 16]);
		expect(buf.readString(16, { encoding: "ascii" })).toEqual("Wolf1 Map1");
		expect(buf.readWords(2)).toEqual([64, 64]);

		expect(buf.readWord(planeDataOffset)).toEqual(1);
		expect(getPlaneXYFromBuffer(buf, 0, 13, 21, 64, 64, planeDataOffset)).toEqual(6);
		expect(getPlaneXYFromBuffer(buf, 0, 34, 38, 64, 64, planeDataOffset)).toEqual(91);
		expect(getPlaneXYFromBuffer(buf, 0, 52, 42, 64, 64, planeDataOffset)).toEqual(118);
		expect(getPlaneXYFromBuffer(buf, 0, 30, 22, 64, 64, planeDataOffset)).toEqual(10);
		expect(getPlaneXYFromBuffer(buf, 0, 63, 63, 64, 64, planeDataOffset)).toEqual(1);

		expect(getPlaneXYFromBuffer(buf, 1, 0, 0, 64, 64, planeDataOffset)).toEqual(0);
		expect(getPlaneXYFromBuffer(buf, 1, 10, 19, 64, 64, planeDataOffset)).toEqual(0);
		expect(getPlaneXYFromBuffer(buf, 1, 52, 44, 64, 64, planeDataOffset)).toEqual(97);
		expect(getPlaneXYFromBuffer(buf, 1, 29, 33, 64, 64, planeDataOffset)).toEqual(27);
		expect(getPlaneXYFromBuffer(buf, 1, 31, 57, 64, 64, planeDataOffset)).toEqual(124);
	});

	it(
		"Writes WDC map to file",
		async () => {
			const inputPath = path.resolve(process.cwd(), "assets/output/wdcmap1.map");
			if (fs.existsSync(inputPath)) {
				fs.unlinkSync(inputPath);
			}

			const filePath = await writeWdcMapFile(inputPath, maps, { maxMapNameBytes: 16, numPlanes: 3 });
			const rereadMaps = await readWdcMapFile(filePath);

			expect(rereadMaps.length).toEqual(1);
			testWolf1Map1(rereadMaps[0]);
		},
		isCIEnv() ? 20000 : undefined
	);
});
