import * as fs from "fs-extra";
import * as path from "path";

import { WolfBuffer } from "../../buffer/WolfBuffer";

export class WolfFileWriter {
	position: number;
	readonly path: string;

	private _fd?: number;

	constructor(path: string) {
		this.path = path;
		this.position = 0;
	}

	async writeFromBuffer(buffer: WolfBuffer, numSegments: number, segmentLength: number = 1, bufferPos: number = 0): Promise<WolfBuffer> {
		const size = numSegments * segmentLength;
		const { buffer: realBuffer } = buffer;
		this.position = Math.max(0, this.position);
		const { position, fd } = this;
		this.position += size;
		await fs.write(fd, realBuffer, bufferPos, size, position);
		return buffer;
	}

	async writeSegmentsFromBuffer(
		buffer: WolfBuffer,
		numSegments: number,
		segmentLength: number = 1,
		bufferPos: number = 0
	): Promise<WolfBuffer> {
		return this.writeFromBuffer(buffer, numSegments, segmentLength, bufferPos);
	}

	async writeBytesFromBuffer(buffer: WolfBuffer, numBytes: number, bufferPos: number = 0): Promise<WolfBuffer> {
		return this.writeSegmentsFromBuffer(buffer, numBytes, 1, bufferPos);
	}

	async writeWordsFromBuffer(buffer: WolfBuffer, numWords: number, bufferPos: number = 0): Promise<WolfBuffer> {
		return this.writeSegmentsFromBuffer(buffer, numWords, WolfBuffer.WORD_SEGMENT_LENGTH, bufferPos);
	}

	async writeDWordsFromBuffer(buffer: WolfBuffer, numDWords: number, bufferPos: number = 0): Promise<WolfBuffer> {
		return this.writeSegmentsFromBuffer(buffer, numDWords, WolfBuffer.DWORD_SEGMENT_LENGTH, bufferPos);
	}

	async writeSegment(segment: number, segmentLength: number = 1): Promise<number> {
		const buffer = WolfBuffer.alloc(segmentLength);
		const result = buffer.writeSegment(segment, segmentLength);
		await this.writeSegmentsFromBuffer(buffer, 1, segmentLength);
		return result;
	}

	async writeByte(byte: number): Promise<number> {
		return this.writeSegment(byte);
	}

	async writeWord(word: number): Promise<number> {
		return this.writeSegment(word, WolfBuffer.WORD_SEGMENT_LENGTH);
	}

	async writeDWord(dword: number): Promise<number> {
		return this.writeSegment(dword, WolfBuffer.DWORD_SEGMENT_LENGTH);
	}

	async writeSegments(segments: number[], segmentLength: number = 1): Promise<number[]> {
		const { length: numSegments } = segments;
		const buffer = WolfBuffer.alloc(numSegments * segmentLength);
		buffer.writeSegments(segments, segmentLength, 0);
		return (await this.writeSegmentsFromBuffer(buffer, segments.length, segmentLength)).readSegments(numSegments, segmentLength, 0);
	}

	async writeBytes(bytes: number[]): Promise<number[]> {
		return this.writeSegments(bytes);
	}

	async writeWords(words: number[]): Promise<number[]> {
		return this.writeSegments(words, WolfBuffer.WORD_SEGMENT_LENGTH);
	}

	async writeDWords(dwords: number[]): Promise<number[]> {
		return this.writeSegments(dwords, WolfBuffer.DWORD_SEGMENT_LENGTH);
	}

	async writeString(str: string, encoding?: BufferEncoding): Promise<string> {
		const buf = Buffer.from(str, encoding);
		const byteLength = buf.byteLength;
		this.position = Math.max(0, this.position);
		const { position, fd } = this;
		this.position += byteLength;
		await fs.write(fd, buf, position, byteLength);
		return str;
	}

	protected get fd(): number {
		if (this._fd == null) {
			fs.mkdirSync(path.dirname(this.path), { recursive: true });
			this._fd = fs.openSync(this.path, "w");
		}
		return this._fd;
	}

	async close(): Promise<void> {
		const { _fd } = this;
		if (_fd == null) {
			return;
		}
		await fs.close(_fd);
	}
}
