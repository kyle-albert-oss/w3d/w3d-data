import { WolfFileWriter } from "./WolfFileWriter";
import { IWadLumpPointer } from "./WadFileReader";
import { WadType } from "./constants";
import { WolfBuffer } from "../../buffer/WolfBuffer";
import { isValidLumpName, LUMP_NAME_ERROR_HINT } from "./wads";

export interface IWadDirectory {
	addPointer(pointer: IWadLumpPointer): IWadLumpPointer;
	writeLump(name: string, data: WolfBuffer): Promise<IWadLumpPointer>;
	end(close?: boolean): Promise<void>;
}

export class WadFileWriter {
	protected wadType: WadType | undefined;
	protected closed: boolean = false;
	protected pointers: IWadLumpPointer[] = [];

	constructor(protected wfw: WolfFileWriter) {}

	get position() {
		return this.wfw.position;
	}

	begin(wadType: WadType): IWadDirectory {
		if (this.closed) {
			throw new Error(`Already closed!`);
		}
		this.wfw.position = 12; // 12 byte header reserved
		this.wadType = wadType;
		return {
			addPointer: this.addPointer,
			writeLump: this.writeLump,
			end: this.end,
		};
	}

	protected addPointer = (pointer: IWadLumpPointer): IWadLumpPointer => {
		this.pointers.push(pointer);
		return pointer;
	};

	/**
	 * Writes a lump and adds a pointer for it. Returns the created pointer. Pointers are written to the file when end() is called.
	 * @param lumpName
	 * @param lump
	 */
	protected writeLump = async (lumpName: string, lump: WolfBuffer): Promise<IWadLumpPointer> => {
		if (!isValidLumpName(lumpName)) {
			throw new Error(`${lumpName} is an invalid lump name. ${LUMP_NAME_ERROR_HINT}`);
		}
		const { wfw } = this;
		const lumpOffset = wfw.position;
		const lumpNumBytes = lump.buffer.byteLength;
		await wfw.writeBytesFromBuffer(lump, lumpNumBytes);
		return this.addPointer({
			lumpName,
			lumpNumBytes,
			lumpOffset,
		});
	};

	/**
	 * Writes the WAD directory lump pointers and WAD file header. After invocation the wad file is complete.
	 * @param close true to close the file writer.
	 */
	protected end = async (close?: boolean): Promise<void> => {
		const { wfw, pointers, wadType } = this;

		// write the directory
		const dirOffset = wfw.position;
		const numPointers = pointers.length;
		const buf = WolfBuffer.allocUnsafe(16 * Math.max(1, numPointers));
		for (const pointer of pointers) {
			const { lumpName, lumpNumBytes, lumpOffset } = pointer;
			buf.writeDWords([lumpOffset, lumpNumBytes]);
			buf.writeString(lumpName, { encoding: "ascii", fixedByteLength: 8 });
		}

		await wfw.writeBytesFromBuffer(buf, buf.buffer.byteLength);

		// write the header
		wfw.position = 0;
		buf.position = 0;
		buf.writeString(wadType!, { fixedByteLength: 4 });
		buf.writeDWords([numPointers, dirOffset]);

		await wfw.writeBytesFromBuffer(buf, 12);
		if (close) {
			await wfw.close();
		}
	};
}
