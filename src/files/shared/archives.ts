import * as path from "path";

import archiver from "archiver";
import * as fs from "fs-extra";
import { Extract } from "unzipper";

/**
 * Unzips a zip file to the destination path. Paths are resolved from process.cwd().
 * @param archivePath
 * @param destinationPath
 */
export const unzipToFolder = async (archivePath: string, destinationPath: string): Promise<string> =>
	new Promise((r, rj) => {
		archivePath = path.resolve(process.cwd(), archivePath);
		destinationPath = path.resolve(process.cwd(), destinationPath);
		fs.mkdirpSync(destinationPath);
		fs.createReadStream(archivePath)
			.pipe(Extract({ path: destinationPath }))
			.on("error", rj)
			.on("close", () => r(destinationPath));
	});

export interface IZipOpts {
	archiveOpts?: archiver.ArchiverOptions;
	archiver?(archiver: archiver.Archiver, srcPath: string): Promise<void>;
	destPath: string;
	srcPath: string;
}

/**
 * Creates a zip file from a folder. The folder contents will be at the root of the zip file. Paths are resolved from process.cwd().
 * @param opts
 * @return The destination path.
 */
export const zipFolder = async (opts: IZipOpts): Promise<string> =>
	new Promise((r, rj) => {
		let { archiveOpts, destPath, srcPath } = opts;
		srcPath = path.resolve(process.cwd(), srcPath);
		destPath = path.resolve(process.cwd(), destPath);
		fs.mkdirpSync(path.dirname(destPath));
		const ws = fs
			.createWriteStream(destPath)
			.on("error", rj)
			.on("close", () => r(destPath));
		const archive = archiver("zip", archiveOpts);
		archive.pipe(ws);
		if (opts.archiver) {
			void opts.archiver(archive, srcPath).then(() => archive.finalize());
		} else {
			archive.directory(srcPath, false);
			void archive.finalize();
		}
	});
