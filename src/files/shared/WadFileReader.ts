import { WolfFileReader } from "./WolfFileReader";
import { WolfBuffer } from "../../buffer/WolfBuffer";
import { WadType } from "./constants";

export interface IWadHeader {
	// not IWAD, just an interface...
	dirOffset: number;
	numFiles: number;
	wadType: WadType;
}

export interface IWadLumpPointer {
	lumpName: string;
	lumpNumBytes: number;
	lumpOffset: number;
}

export interface IWadLump {
	data: WolfBuffer;
	pointer: IWadLumpPointer;
}

export class WadFileReader {
	constructor(protected wfr: WolfFileReader) {}

	async readHeader(): Promise<IWadHeader> {
		const { wfr } = this;
		wfr.position = 0;
		const buf = await wfr.readBytesIntoBuffer(WolfBuffer.alloc(12), 12);

		const wadType = buf.readString(4, { encoding: "ascii" });
		if (wadType !== "PWAD" && wadType !== "IWAD") {
			throw new Error(`Invalid WAD file '${wfr.path.toString()}': expected PWAD or IWAD in header but found ${wadType}.`);
		}
		const [numFiles, dirOffset] = buf.readDWords(2);

		return {
			dirOffset,
			numFiles,
			wadType,
		};
	}

	async readLumpPointers(header: IWadHeader): Promise<IWadLumpPointer[]> {
		const { wfr } = this;
		const { dirOffset, numFiles } = header;
		const result: IWadLumpPointer[] = [];

		const buf = WolfBuffer.alloc(16);
		wfr.position = dirOffset;
		for (let fileIdx = 0; fileIdx < numFiles; fileIdx++) {
			await wfr.readBytesIntoBuffer(buf, 16);
			buf.position = 0;
			const [lumpOffset, lumpNumBytes] = buf.readDWords(2);
			const lumpName = buf.readString(8, { encoding: "ascii" });
			result.push({
				lumpName,
				lumpNumBytes,
				lumpOffset,
			});
		}

		return result;
	}

	async readEntireLump(pointer: IWadLumpPointer): Promise<IWadLump> {
		const { wfr } = this;
		const { lumpOffset, lumpNumBytes } = pointer;

		wfr.position = lumpOffset;
		const data = await wfr.readBytesIntoBuffer(WolfBuffer.alloc(lumpNumBytes), lumpNumBytes);

		return {
			data,
			pointer,
		};
	}

	async close(): Promise<void> {
		return this.wfr.close();
	}
}
