import * as path from "path";
import { WolfBuffer } from "../../../buffer/WolfBuffer";

import { WolfFileReader } from "../WolfFileReader";

const testDataPath = path.resolve(process.cwd(), "assets/wl6/GAMEMAPS.WL6");

it("initializes", () => {
	new WolfFileReader(testDataPath);
});

it("reads first byte", async () => {
	const wrf = new WolfFileReader(testDataPath);
	const firstByte = await wrf.readByte();

	expect(firstByte).toEqual(0x54);
});

it("reads first word", async () => {
	const wrf = new WolfFileReader(testDataPath);
	const firstWord = await wrf.readWord();

	expect(firstWord).toEqual(0x4554);
});

it("reads first dword", async () => {
	const wrf = new WolfFileReader(testDataPath);
	const firstWord = await wrf.readDWord();

	expect(firstWord).toEqual(0x35444554);
});

it("reads word at set offset", async () => {
	const wrf = new WolfFileReader(testDataPath);
	wrf.position = 1;
	const word = await wrf.readWord();

	expect(word).toEqual(0x4445);
});

it("maintains position", async () => {
	const wrf = new WolfFileReader(testDataPath);

	let content: number | number[] = await wrf.readByte();
	expect(wrf.position).toEqual(1);

	content = await wrf.readWord();
	expect(wrf.position).toEqual(3);

	content = await wrf.readDWord();
	expect(wrf.position).toEqual(7);

	const buf = new WolfBuffer(Buffer.alloc(1000));

	await wrf.readBytesIntoBuffer(buf, 3);
	expect(wrf.position).toEqual(10);

	await wrf.readWordsIntoBuffer(buf, 2);
	expect(wrf.position).toEqual(14);

	await wrf.readDWordsIntoBuffer(buf, 3);
	expect(wrf.position).toEqual(26);
});
