import { isValidLumpName, isValidWadFileName } from "../wads";

describe("Valid Lump Names", () => {
	it("Passes for valid lump names.", () => {
		expect(isValidLumpName("ABC123")).toBeTruthy();
		expect(isValidLumpName("12345678")).toBeTruthy();
		expect(isValidLumpName("QWERTYUI")).toBeTruthy();
		expect(isValidLumpName("[]\\-_AB")).toBeTruthy();
	});

	it("Fails for invalid lump names.", () => {
		expect(isValidLumpName("abc")).toBeFalsy();
		expect(isValidLumpName("ABcD123")).toBeFalsy();
		expect(isValidLumpName("TEST!")).toBeFalsy();
		expect(isValidLumpName("TEST🚀")).toBeFalsy();
		expect(isValidLumpName("")).toBeFalsy();
		expect(isValidLumpName("ABCDEFGHI")).toBeFalsy();
	});
});

describe("Valid Wad File Names", () => {
	it("Passes for valid wad file names.", () => {
		expect(isValidWadFileName("ABc123")).toBeTruthy();
		expect(isValidWadFileName("12345678")).toBeTruthy();
		expect(isValidWadFileName("QWERTYUI")).toBeTruthy();
		expect(isValidWadFileName("[]-_AB")).toBeTruthy();
		expect(isValidWadFileName("e1m1")).toBeTruthy();
	});

	it("Fails for invalid wad file names.", () => {
		expect(isValidWadFileName("TEST\\")).toBeFalsy();
		expect(isValidWadFileName("TEST!")).toBeFalsy();
		expect(isValidWadFileName("TEST🚀")).toBeFalsy();
		expect(isValidWadFileName("")).toBeFalsy();
		expect(isValidWadFileName("ABCDEFGHI")).toBeFalsy();
		// reserved windows file names.
		expect(isValidWadFileName("CON")).toBeFalsy();
		expect(isValidWadFileName("COM1")).toBeFalsy();
	});
});
