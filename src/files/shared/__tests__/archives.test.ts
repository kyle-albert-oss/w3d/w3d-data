import * as fs from "fs-extra";
import { unzipToFolder, zipFolder } from "../archives";
import * as path from "path";
import { isCIEnv } from "../../../utils/tests";
import { readJascPaletteFileColors } from "../../palettes/jasc";

describe("Archives", () => {
	it(
		"Unzips a folder correctly",
		async () => {
			const outputDir = "assets/output/zips/lzwolf";
			fs.removeSync(path.resolve(process.cwd(), outputDir));

			expect(fs.existsSync(outputDir)).toBeFalsy();

			const result = await unzipToFolder("assets/lzwolf/lzwolf.pk3", outputDir);

			expect(path.relative(process.cwd(), result)).toEqual("assets/output/zips/lzwolf");
			expect(fs.existsSync(path.resolve(result, "wl6map.txt")));
			expect(fs.existsSync(path.resolve(result, "xlat/wolf3d.txt")));
		},
		isCIEnv() ? 20000 : undefined
	);

	it(
		"Zips a folder correctly",
		async () => {
			const outputDir = "assets/output/zips/archive.zip";
			fs.removeSync(path.resolve(process.cwd(), outputDir));

			expect(fs.existsSync(outputDir)).toBeFalsy();
			const inputColors = await readJascPaletteFileColors(path.resolve(process.cwd(), "assets/palettes/Wolf3D.pal"));
			expect(inputColors.length).toEqual(256);
			expect(inputColors[255]).toEqual([152, 0, 136]);

			const result = await zipFolder({ srcPath: "assets/palettes", destPath: outputDir });

			expect(fs.existsSync(result)).toBeTruthy();

			const unzippedResult = await unzipToFolder(outputDir, "assets/output/zips/archive");
			const resultColors = await readJascPaletteFileColors(path.resolve(unzippedResult, "Wolf3D.pal"));
			expect(resultColors.length).toEqual(256);
			expect(resultColors[255]).toEqual([152, 0, 136]);
		},
		isCIEnv() ? 20000 : undefined
	);
});
