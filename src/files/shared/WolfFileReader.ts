import * as fs from "fs-extra";

import { WolfBuffer } from "../../buffer/WolfBuffer";

export class WolfFileReader {
	position: number;
	readonly path: fs.PathLike;

	private _fd?: number;
	private _stats?: fs.Stats;

	constructor(path: fs.PathLike) {
		this.path = path;
		this.position = 0;
	}

	async readIntoBuffer(buffer: WolfBuffer, numSegments: number, segmentLength: number = 1, bufferPos: number = 0): Promise<WolfBuffer> {
		const size = numSegments * segmentLength;
		const { buffer: realBuffer } = buffer;
		const position = Math.max(0, this.position);
		const { fd } = this;
		this.position += size;
		await fs.read(fd, realBuffer, bufferPos, size, position);
		return buffer;
	}

	async readBytesIntoBuffer(buffer: WolfBuffer, numBytes: number, bufferPos: number = 0): Promise<WolfBuffer> {
		return this.readIntoBuffer(buffer, numBytes, 1, bufferPos);
	}

	async readWordsIntoBuffer(buffer: WolfBuffer, numWords: number, bufferPos: number = 0): Promise<WolfBuffer> {
		return this.readIntoBuffer(buffer, numWords, WolfBuffer.WORD_SEGMENT_LENGTH, bufferPos);
	}

	async readDWordsIntoBuffer(buffer: WolfBuffer, numDWords: number, bufferPos: number = 0): Promise<WolfBuffer> {
		return this.readIntoBuffer(buffer, numDWords, WolfBuffer.DWORD_SEGMENT_LENGTH, bufferPos);
	}

	async readSegmentsWithBuffer(
		buffer: WolfBuffer,
		numSegments: number,
		segmentLength: number = 1,
		bufferPos: number = 0
	): Promise<number[]> {
		return (await this.readIntoBuffer(buffer, numSegments, segmentLength, bufferPos)).readSegments(numSegments, segmentLength, bufferPos);
	}

	async readBytesWithBuffer(buffer: WolfBuffer, numBytes: number, bufferPos: number = 0): Promise<number[]> {
		return (await this.readIntoBuffer(buffer, numBytes, 1, bufferPos)).readSegments(numBytes, 1, bufferPos);
	}

	async readByteWithBuffer(buffer: WolfBuffer, bufferPos: number = 0): Promise<number> {
		return (await this.readIntoBuffer(buffer, 1, 1, bufferPos)).readByte(bufferPos);
	}

	async readWordsWithBuffer(buffer: WolfBuffer, numWords: number, bufferPos: number = 0): Promise<number[]> {
		return (await this.readIntoBuffer(buffer, numWords, WolfBuffer.WORD_SEGMENT_LENGTH, bufferPos)).readSegments(
			numWords,
			WolfBuffer.WORD_SEGMENT_LENGTH,
			bufferPos
		);
	}

	async readWordWithBuffer(buffer: WolfBuffer, bufferPos: number = 0): Promise<number> {
		return (await this.readIntoBuffer(buffer, 1, WolfBuffer.WORD_SEGMENT_LENGTH, bufferPos)).readWord(bufferPos);
	}

	async readDWordsWithBuffer(buffer: WolfBuffer, numDWords: number, bufferPos: number = 0): Promise<number[]> {
		return (await this.readIntoBuffer(buffer, numDWords, WolfBuffer.DWORD_SEGMENT_LENGTH, bufferPos)).readSegments(
			numDWords,
			WolfBuffer.DWORD_SEGMENT_LENGTH,
			bufferPos
		);
	}

	async readDWordWithBuffer(buffer: WolfBuffer, bufferPos: number = 0): Promise<number> {
		return (await this.readIntoBuffer(buffer, 1, WolfBuffer.DWORD_SEGMENT_LENGTH, bufferPos)).readDWord(bufferPos);
	}

	async readStringWithBuffer(buffer: WolfBuffer, numBytes: number, encoding?: BufferEncoding, bufferPos: number = 0): Promise<string> {
		return (await this.readBytesIntoBuffer(buffer, numBytes, bufferPos)).readString(numBytes, { encoding }, bufferPos);
	}

	async readByte(): Promise<number> {
		return (await this.readIntoBuffer(WolfBuffer.alloc(1), 1)).readByte();
	}

	async readWord(): Promise<number> {
		return (await this.readIntoBuffer(WolfBuffer.alloc(WolfBuffer.WORD_SEGMENT_LENGTH), 1, WolfBuffer.WORD_SEGMENT_LENGTH)).readWord();
	}

	async readDWord(): Promise<number> {
		return (await this.readIntoBuffer(WolfBuffer.alloc(WolfBuffer.DWORD_SEGMENT_LENGTH), 1, WolfBuffer.DWORD_SEGMENT_LENGTH)).readDWord();
	}

	async readBytes(numBytes: number): Promise<number[]> {
		return (await this.readBytesIntoBuffer(WolfBuffer.alloc(numBytes), numBytes)).readBytes(numBytes);
	}

	async readWords(numWords: number): Promise<number[]> {
		return (await this.readWordsIntoBuffer(WolfBuffer.alloc(numWords * WolfBuffer.WORD_SEGMENT_LENGTH), numWords)).readWords(numWords);
	}

	async readDWords(numDWords: number): Promise<number[]> {
		return (await this.readDWordsIntoBuffer(WolfBuffer.alloc(numDWords * WolfBuffer.DWORD_SEGMENT_LENGTH), numDWords)).readDWords(
			numDWords
		);
	}

	async readString(length: number, encoding?: BufferEncoding): Promise<string> {
		return this.readStringWithBuffer(WolfBuffer.alloc(length), length, encoding);
	}

	protected get fd(): number {
		if (this._fd == null) {
			this._fd = fs.openSync(this.path, "r");
		}
		return this._fd;
	}

	get stats(): fs.Stats {
		if (this._stats == null) {
			this._stats = fs.statSync(this.path);
		}
		return this._stats;
	}

	get fileSize(): number {
		return this.stats.size;
	}

	async close(): Promise<void> {
		const { _fd } = this;
		this._fd = undefined;
		this._stats = undefined;
		if (_fd == null) {
			return;
		}
		await fs.close(_fd);
	}
}
