import validFilename from "valid-filename";
import { getFileNameNoExt } from "../../utils/files";

/**
 * @see https://zdoom.org/wiki/WAD
 */
// eslint-disable-next-line no-useless-escape
export const LUMP_NAME_VALID_CHARS_REGEX = /^[A-Z0-9_\[\]\-\\]+$/;
export const LUMP_NAME_ERROR_HINT = `Lump names can only have characters A-Z (case-sensitive), 0-9, [, ], -, _, \\ and be no longer than 8 ASCII characters.`;
// eslint-disable-next-line no-useless-escape
export const WAD_FILE_NAME_VALID_CHARS_REGEX = /^[A-Z0-9_\[\]\-\\]+$/i;
export const WAD_FILE_NAME_ERROR_HINT = `WAD file names can only have characters A-Z (case-insensitive), 0-9, [, ], -, _, be no longer than 8 ASCII characters, and cannot be a reserved filename.`;

export const isValidLumpName = (name: string) => {
	if (!LUMP_NAME_VALID_CHARS_REGEX.test(name)) {
		return false;
	}
	const byteLength = Buffer.byteLength(name, "ascii");
	return byteLength > 0 && byteLength <= 8;
};

export const isValidWadFileName = (pathOrName: string) => {
	const fileName = getFileNameNoExt(pathOrName);
	if (!WAD_FILE_NAME_VALID_CHARS_REGEX.test(fileName) || !validFilename(fileName)) {
		return false;
	}
	const byteLength = Buffer.byteLength(fileName, "ascii");
	return byteLength > 0 && byteLength <= 8;
};
