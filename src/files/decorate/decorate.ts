import * as fs from "fs-extra";
import nearley from "nearley";
import GRAMMAR_DECORATE from "../../grammar/parsers/decorate-parser";

const newDecorateParser = () => new nearley.Parser(nearley.Grammar.fromCompiled(GRAMMAR_DECORATE));

export const readDecorateFile = async (filePath: string | Buffer | number, parser: nearley.Parser = newDecorateParser()) =>
	parser.feed(await fs.readFile(filePath, "utf8")).results;
