import * as fs from "fs";
import * as path from "path";
import diff from "jest-diff";
import { readDecorateFile } from "../decorate";

let result: any[];

const findDifferenceArr = (obj1: any, obj2: any[]) => {
	let diffResult: string = "";
	for (let i = 0; i < obj2.length; i++) {
		const tmp = diff(JSON.stringify(obj1, undefined, "\t"), JSON.stringify(obj2[i], undefined, "\t"));
		if (!tmp?.includes("Compared values have no visual difference.")) {
			diffResult += "\n\n" + (tmp ?? "NULL");
		}
	}
	return diffResult || "No differences in any.";
};

const findActorBlock = (nodes: any[], actorName: string) =>
	result.find((n) => n?.type === "block" && n?.header?.type?.value === "actor" && n?.header?.name?.value === actorName);

const childFilePaths = (dir: string): [string, string][] => {
	const inputDir = dir;
	dir = path.resolve(process.cwd(), dir);
	return fs.readdirSync(dir).map((f) => [inputDir, f]);
};

const baseFilePath = "assets/lzwolf/exploded";

beforeAll(() => {
	expect(fs.existsSync(path.resolve(process.cwd(), baseFilePath))).toBeTruthy();
});

describe("Does it read and is not ambiguous?", () => {
	describe("wolf", () => {
		test.each(childFilePaths(`${baseFilePath}/actors/wolf`))("%s/%s", async (dirPath: string, fileName: string) => {
			result = await readDecorateFile(path.relative(process.cwd(), path.join(dirPath, fileName)));
			if (result.length > 1) {
				// console.log(findDifferenceArr(result[0], result.slice(1)));
				fail(new Error("Grammar is ambiguous"));
			} else {
				result = result[0];
			}
		});
	});

	// TODO: reenable
	describe.skip("blake", () => {
		test.each(childFilePaths(`${baseFilePath}/actors/blake`))("%s/%s", async (dirPath: string, fileName: string) => {
			result = await readDecorateFile(path.relative(process.cwd(), path.join(dirPath, fileName)));
			if (result.length > 1) {
				// console.log(findDifferenceArr(result[0], result.slice(1)));
				fail(new Error("Grammar is ambiguous"));
			} else {
				result = result[0];
			}
		});
	});

	// TODO: reenable
	describe.skip("noah", () => {
		test.each(childFilePaths(`${baseFilePath}/actors/noah`))("%s/%s", async (dirPath: string, fileName: string) => {
			result = await readDecorateFile(path.relative(process.cwd(), path.join(dirPath, fileName)));
			if (result.length > 1) {
				// console.log(findDifferenceArr(result[0], result.slice(1)));
				fail(new Error("Grammar is ambiguous"));
			} else {
				result = result[0];
			}
		});
	});
});

describe("wolfguards.txt", () => {
	beforeAll(async () => {
		const decorateFilePath = path.resolve(process.cwd(), `${baseFilePath}/actors/wolf/wolfguards.txt`);
		result = await readDecorateFile(decorateFilePath);
		if (result.length > 1) {
			// console.log(findDifferenceArr(result[0], result.slice(1)));
			fail(new Error("Grammar is ambiguous"));
		} else {
			result = result[0];
		}
	});

	describe("actor WolfensteinMonster", () => {
		let block: any;
		let body: any[];

		beforeAll(() => {
			block = findActorBlock(result, "WolfensteinMonster");
			expect(block).toBeTruthy();

			body = block.body;
			expect(body.length).toEqual(11);
		});

		it("Correctly parses the actor block header", () => {
			expect("extends" in block?.header).toBeTruthy();
			expect("name" in block?.header).toBeTruthy();
			expect("replaces" in block?.header).toBeTruthy();
			expect("tileNum" in block?.header).toBeTruthy();
			expect("type" in block?.header).toBeTruthy();

			expect(block.header?.type?.value).toEqual("actor");
			expect(block.header?.name?.value).toEqual("WolfensteinMonster");
			expect(block.header?.extends).toBeUndefined();
			expect(block.header?.replaces).toBeUndefined();
			expect(block.header?.tileNum?.value).toBeUndefined();
		});

		it("Correctly parses simple property value statement.", () => {
			expect(body[0].type).toEqual("property");
			expect(body[0].name.type).toEqual("property-name");
			expect(body[0].name.path[0].value).toEqual("missilefrequency");
			expect(body[0].name.pathStr).toEqual("missilefrequency");
			expect(body[0].valueExpr.type).toEqual("number");
			expect(body[0].valueExpr.value).toEqual(0.08);
		});

		it("Correctly parses standalone property name.", () => {
			expect(body[7].type).toEqual("property-name");
			expect(body[7].path[0].value).toEqual("MONSTER");
			expect(body[7].pathStr).toEqual("MONSTER");
		});

		it("Correctly parses flag.", () => {
			expect(body[8].type).toEqual("flag");
			expect(body[8].value).toBeTruthy();
			expect(body[8].value.modifier).toEqual("+");
			expect(body[8].value.pathStr).toEqual("ALWAYSFAST");
			expect(body[8].value.path[0]).toEqual("ALWAYSFAST");
		});
	});

	describe("actor Schutzstafell", () => {
		let block: any;
		let body: any[];

		beforeAll(() => {
			block = findActorBlock(result, "Schutzstafell");
			expect(block).toBeTruthy();

			body = block.body;
			expect(body.length).toEqual(7);
		});

		it("Correctly parses the actor block header", () => {
			expect("extends" in block?.header).toBeTruthy();
			expect("name" in block?.header).toBeTruthy();
			expect("replaces" in block?.header).toBeTruthy();
			expect("tileNum" in block?.header).toBeTruthy();
			expect("type" in block?.header).toBeTruthy();

			expect(block.header?.type?.value).toEqual("actor");
			expect(block.header?.name?.value).toEqual("Schutzstafell");
			expect(block.header?.extends?.value).toEqual("WolfensteinSS");
			expect(block.header?.replaces).toBeUndefined();
			expect(block.header?.tileNum?.value).toEqual(94);
		});

		it("Correctly parses simple property value statement.", () => {
			expect(body[1].type).toEqual("property");
			expect(body[1].name.type).toEqual("property-name");
			expect(body[1].name.path[0].value).toEqual("deathsound");
			expect(body[1].name.pathStr).toEqual("deathsound");
			expect(body[1].valueExpr.type).toEqual("string");
			expect(body[1].valueExpr.value).toEqual("ss/death");
		});

		describe("States", () => {
			let block: any;
			let states: any[];

			beforeAll(() => {
				block = body[6];
				expect(block).toBeTruthy();

				states = block.body;
				expect(states?.length).toEqual(40);
			});

			it("Correctly parses all state labels.", () => {
				expect(states.filter((n) => n?.type === "state-label").map((n) => n.value)).toEqual([
					"Spawn",
					"Path",
					"See",
					"Pain",
					"Missile",
					"Death",
				]);
			});

			it("Correctly parses state sprite", () => {
				expect(states[1].name.value).toEqual("SSFL");
			});

			it("Correctly parses simple control flow.", () => {
				expect(states[2].type).toEqual("control-flow");
				expect(states[2].keyword.value).toEqual("stop");
			});

			it("Correctly parses goto with state label.", () => {
				expect(states[21].type).toEqual("control-flow");
				expect(states[21].keyword.value).toEqual("goto");
				expect(states[21].nextState.value).toEqual("See");
				expect("nextStateOffset" in states[21]).toBeTruthy();
				expect(states[21].nextStateOffset).toBeUndefined();
			});

			it("Correctly parses state def with a simple action function.", () => {
				expect(states[6].type).toEqual("state-def");
				expect(states[6].duration.type).toEqual("tics");
				expect(states[6].duration.value.type).toEqual("number");
				expect(states[6].duration.value.value).toEqual(7.5);
				expect(states[6].frames.value).toEqual("B");
				expect(states[6].keyword.value).toEqual("NOP");
				expect(states[6].actionFns.length).toEqual(1);
				expect(states[6].actionFns[0].type).toEqual("action-fn");
				expect(states[6].actionFns[0].identifier.type).toEqual("action_identifier");
				expect(states[6].actionFns[0].identifier.value).toEqual("A_Chase");
				expect(states[6].actionFns[0].args).toEqual([]);
			});

			it("Correctly parses state def with an action function with arguments.", () => {
				expect(states[20].type).toEqual("state-def");
				expect(states[20].duration.type).toEqual("tics");
				expect(states[20].duration.value.type).toEqual("number");
				expect(states[20].duration.value.value).toEqual(5);
				expect(states[20].frames.value).toEqual("H");
				expect("keyword" in states[20]).toBeTruthy();
				expect(states[20].keyword).toBeUndefined();
				expect(states[20].actionFns.length).toEqual(1);
				expect(states[20].actionFns[0].type).toEqual("action-fn");
				expect(states[20].actionFns[0].identifier.type).toEqual("action_identifier");
				expect(states[20].actionFns[0].identifier.value).toEqual("A_JumpIf");
				expect(Array.isArray(states[20].actionFns[0].args)).toBeTruthy();
				expect(states[20].actionFns[0].args.length).toEqual(2);

				// args
				const [arg1, arg2] = states[20].actionFns[0].args;

				expect(arg1.type).toEqual("binary-op");
				expect(arg1.left.type).toEqual("identifier");
				expect(arg1.left.value).toEqual("health");
				expect(arg1.operator.value).toEqual("&");
				expect(arg1.right.value).toEqual(1);

				expect(arg2.type).toEqual("number");
				expect(arg2.value).toEqual(1);
			});
		});
	});
});

describe("wolfplayer.txt", () => {
	beforeAll(async () => {
		const decorateFilePath = path.resolve(process.cwd(), `${baseFilePath}/actors/wolf/wolfplayer.txt`);
		result = await readDecorateFile(decorateFilePath);
		if (result.length > 1) {
			// console.log(findDifferenceArr(result[0], result.slice(1)));
			fail(new Error("Grammar is ambiguous"));
		} else {
			result = result[0];
		}
	});

	describe("actor BJPlayer", () => {
		let block: any;
		let body: any[];

		beforeAll(() => {
			block = findActorBlock(result, "BJPlayer");
			expect(block).toBeTruthy();

			body = block.body;
			expect(body.length).toEqual(14);
		});

		it("Correctly parses the actor block header", () => {
			expect("extends" in block?.header).toBeTruthy();
			expect("name" in block?.header).toBeTruthy();
			expect("replaces" in block?.header).toBeTruthy();
			expect("tileNum" in block?.header).toBeTruthy();
			expect("type" in block?.header).toBeTruthy();

			expect(block.header?.type?.value).toEqual("actor");
			expect(block.header?.name?.value).toEqual("BJPlayer");
			expect(block.header?.extends?.value).toEqual("PlayerPawn");
			expect(block.header?.replaces).toBeUndefined();
			expect(block.header?.tileNum?.value).toBeUndefined();
		});

		it("Correctly parses property path with value statement.", () => {
			expect(body[5].type).toEqual("property");
			expect(body[5].name.type).toEqual("property-name");
			expect(body[5].name.pathStr).toEqual("player.startitem");
			expect(body[5].name.path[0].value).toEqual("player");
			expect(body[5].name.path[1].value).toEqual("startitem");
			expect(body[5].valueExpr.type).toEqual("expr-list");
			expect(body[5].valueExpr.values.length).toEqual(2);
			expect(body[5].valueExpr.values[0].value).toEqual("Clip");
			expect(body[5].valueExpr.values[1].value).toEqual(8);
		});
	});

	describe("actor BJRun", () => {
		let block: any;
		let body: any[];

		beforeAll(() => {
			block = findActorBlock(result, "BJRun");
			expect(block).toBeTruthy();

			body = block.body;
			expect(body.length).toEqual(4);
		});

		it("Correctly parses the actor block header", () => {
			expect("extends" in block?.header).toBeTruthy();
			expect("name" in block?.header).toBeTruthy();
			expect("replaces" in block?.header).toBeTruthy();
			expect("tileNum" in block?.header).toBeTruthy();
			expect("type" in block?.header).toBeTruthy();

			expect(block.header?.type?.value).toEqual("actor");
			expect(block.header?.name?.value).toEqual("BJRun");
			expect(block.header?.extends).toBeUndefined();
			expect(block.header?.replaces).toBeUndefined();
			expect(block.header?.tileNum?.value).toBeUndefined();
		});

		it("Correctly parses Exit_Victory.", () => {
			expect(body[3].type).toEqual("block");
			expect(body[3].body.length).toEqual(15);
			expect(body[3].body[13].type).toEqual("state-def");
			expect(body[3].body[13].actionFns.length).toEqual(1);
			expect(body[3].body[13].actionFns[0].type).toEqual("action-fn");
			expect(body[3].body[13].actionFns[0].identifier.value).toEqual("Exit_Victory");
		});
	});
});
