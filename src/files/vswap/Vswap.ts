export interface IVswapGraphicChunkMeta {
	height: number;
	type: "wall" | "sprite";
	width: number;
}

export type VswapChunkMeta = IVswapGraphicChunkMeta;

export interface IVswapGraphicChunk {
	type: "graphic";
	rawData: any;
	meta: IVswapGraphicChunkMeta;
}

export type VswapChunk = IVswapGraphicChunk; // TODO: sound
