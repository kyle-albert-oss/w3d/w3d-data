import * as path from "path";

import { IVswapGraphicChunkMeta } from "../Vswap";
import { IVswapHeader, IVswapIngester, IVswapIngesterConfig, VswapFileReader } from "../VswapFileReader";
import { isCIEnv } from "../../../utils/tests";

const vswap1 = path.resolve(process.cwd(), "assets/wl6/VSWAP.WL6");

interface ITestVswapIngesterResult {
	firstWall?: {
		data: number[];
		meta: IVswapGraphicChunkMeta;
	};
	firstSprite?: {
		data: number[];
		meta: IVswapGraphicChunkMeta;
	};
}

class TestVswapIngester implements IVswapIngester<ITestVswapIngesterResult> {
	firstWall?: ITestVswapIngesterResult["firstWall"];
	firstSprite?: ITestVswapIngesterResult["firstSprite"];

	accept = async (data: number[], pageIndex: number, meta: IVswapGraphicChunkMeta): Promise<void> => {
		if (!this.firstWall && meta.type === "wall") {
			this.firstWall = { data, meta };
		}
		if (!this.firstSprite && meta.type === "sprite") {
			this.firstSprite = { data, meta };
		}
	};

	getConfig(): IVswapIngesterConfig {
		return {
			transparentColorIndex: -1,
		};
	}

	onComplete = async (): Promise<ITestVswapIngesterResult> => {
		return {
			firstWall: this.firstWall,
			firstSprite: this.firstSprite,
		};
	};

	// eslint-disable-next-line @typescript-eslint/no-empty-function
	onInit(header: IVswapHeader): void {}
}

let result: ITestVswapIngesterResult;

beforeAll(
	async () => {
		const vfr = new VswapFileReader(vswap1);
		result = await vfr.getData(new TestVswapIngester());
	},
	isCIEnv() ? 20000 : undefined
);

it("reads", () => {
	//TODO
});
