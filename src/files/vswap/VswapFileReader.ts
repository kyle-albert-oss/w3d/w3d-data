import * as fs from "fs";

import { WolfBuffer } from "../../buffer/WolfBuffer";
import { WolfFileReader } from "../shared/WolfFileReader";
import { VswapChunkMeta } from "./Vswap";
import memoize from "lodash.memoize";

// eslint-disable-next-line @typescript-eslint/unbound-method
const memoSqrt = memoize(Math.sqrt);

export interface IVswapHeader {
	numChunks: number;
	spritePageOffset: number;
	soundPageOffset: number;
	pageOffsets: number[];
	pageLengths: number[];
	calculated: {
		maxPageLength: number;
		maxPageWidth: number;
		maxPageHeight: number;
	};
}

// eslint-disable-next-line
export interface IVswapIngesterConfig {}

export interface IVswapIngester<R> {
	getConfig?(): IVswapIngesterConfig | undefined;
	onInit(header: IVswapHeader): void;
	accept(data: number[], pageIndex: number, meta: VswapChunkMeta): Promise<void>;
	onComplete(): Promise<R>;
}

export class VswapFileReader {
	constructor(private readonly path: fs.PathLike) {}

	getData = async <T>(ingester: IVswapIngester<T>): Promise<T> => {
		const config = ingester.getConfig ? ingester.getConfig() : undefined; // TODO: may use this in the future to do sliced loads or tweak things.

		const vfr = new WolfFileReader(this.path);

		// parse header info
		const [chunks, spritePageOffset, soundPageOffset] = await vfr.readWords(3);
		const graphicChunks = soundPageOffset;

		const pageOffsets = await vfr.readDWords(chunks);
		let dataStart: number = -1;
		for (let x = 0; x < pageOffsets.length; x++) {
			if (x === 0) {
				dataStart = pageOffsets[0];
			}
			if (pageOffsets[x] !== 0 && (pageOffsets[x] < dataStart || pageOffsets[x] > vfr.fileSize)) {
				throw new Error(`VSWAP file '${vfr.path.toString()}' contains invalid page offsets`);
			}
		}
		const pageLengths = await vfr.readWords(chunks);
		const maxPageLength = Math.max(...pageLengths);
		const maxPageWidth = Math.ceil(memoSqrt(maxPageLength));
		// noinspection JSSuspiciousNameCombination,UnnecessaryLocalVariableJS
		const maxPageHeight = maxPageWidth;

		ingester.onInit({
			numChunks: chunks,
			spritePageOffset,
			soundPageOffset,
			pageOffsets,
			pageLengths,
			calculated: {
				maxPageLength,
				maxPageWidth,
				maxPageHeight,
			},
		});

		let page;
		const buf = WolfBuffer.alloc(maxPageLength);

		// read walls
		for (page = 0; page < spritePageOffset; page++) {
			vfr.position = pageOffsets[page];
			const pageLength = pageLengths[page];
			await vfr.readBytesIntoBuffer(buf, pageLengths[page]);

			const width = memoSqrt(pageLength);
			// noinspection JSSuspiciousNameCombination
			const height = width;
			const data: number[] = []; // array of palette indexes

			const bytes = buf.readBytes(pageLength, 0);
			for (let col = 0, z = 0; col < width; col++) {
				for (let row = 0; row < height; row++, z++) {
					// noinspection UnnecessaryLocalVariableJS
					const palIdx = bytes[width * row + col];
					data[z] = palIdx;
				}
			}

			await ingester.accept(data, page, { type: "wall", width, height });
		}

		// read sprites
		for (; page < graphicChunks; page++) {
			const pageLength = pageLengths[page];
			const width = memoSqrt(4096); // TODO: dynamic sprite page sizes/128px?
			// noinspection JSSuspiciousNameCombination
			const height = width;

			vfr.position = pageOffsets[page];
			buf.buffer.fill(0);
			await vfr.readBytesIntoBuffer(buf, pageLength);

			let [leftPix, rightPix] = buf.readWords(2, 0);
			const totalColumns = rightPix - leftPix + 1;
			const colDataOfs = buf.readWords(totalColumns);
			let pixelIdx = totalColumns * 2 + 4;

			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			let startY, endY, newStart;
			const data: number[] = []; // array of palette indexes, if data[x] is undefined, consider it a transparent pixel and leave it up the ingester to decide what to do
			for (let spot = 0; leftPix <= rightPix; leftPix++, spot++) {
				let pos = colDataOfs[spot];
				// tslint:disable-next-line:no-conditional-assignment
				while ((endY = buf.readWord(pos)) !== 0) {
					endY >>= 1;
					[newStart, startY] = buf.readWords(2);
					startY >>= 1;
					pos = buf.position;

					for (; startY < endY; startY++, pixelIdx++) {
						const palIdx = buf.readByte(pixelIdx);
						const z = startY * width + leftPix;
						data[z] = palIdx;
					}
				}
			}

			await ingester.accept(data, page, { type: "sprite", width, height });
		}

		return ingester.onComplete();
	};
}
