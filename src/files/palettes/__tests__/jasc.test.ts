import * as path from "path";
import { readJascPaletteFile, readJascPaletteFileColors } from "../jasc";

const jascPal1 = path.resolve(process.cwd(), "assets/palettes/Wolf3D.pal");

it("Correctly reads the standard Wolf 3D palette", async () => {
	let nodes = await readJascPaletteFile(jascPal1);

	expect(nodes.length).toEqual(1); // > 1 = ambiguous

	nodes = nodes[0];

	expect(nodes.length).toEqual(2); // header + color list
	expect(nodes[0].type).toEqual("header");
	expect(nodes[1].type).toEqual("color-list");
	expect(nodes[1].colors?.length).toEqual(256);
	expect(nodes[1].colors?.[0]).toEqual({ type: "rgb", r: 0, g: 0, b: 0 });
	expect(nodes[1].colors?.[255]).toEqual({ type: "rgb", r: 152, g: 0, b: 136 });
});

it("Correctly reads the standard Wolf 3D palette (colors variant)", async () => {
	const colors = await readJascPaletteFileColors(jascPal1);

	expect(colors.length).toEqual(256);
	expect(colors[0]).toEqual([0, 0, 0]);
	expect(colors[255]).toEqual([152, 0, 136]);
});
