import nearley from "nearley";
import * as fs from "fs-extra";
import GRAMMAR_PAL_JASC from "../../grammar/parsers/jasc-pal-parser";
import { IPaletteFileReaderFn, PaletteColor } from "../../models/palette";

export const newJascPaletteParser = () => new nearley.Parser(nearley.Grammar.fromCompiled(GRAMMAR_PAL_JASC));

export const readJascPaletteFile = async (pathLike: string | Buffer | number, parser = newJascPaletteParser()) =>
	parser.feed(await fs.readFile(pathLike, "utf8")).results;

export const readJascPaletteFileColors: IPaletteFileReaderFn = async (filePath, parser) => {
	const results = await readJascPaletteFile(filePath, parser);
	return (
		(results?.[0].find((n: any) => n?.type === "color-list")?.colors as any[])
			?.filter((n: any) => n?.type === "rgb")
			.map((n: any): PaletteColor => [n.r, n.g, n.b]) ?? []
	);
};
