import { PathLike } from "fs";
import { DeepReadonly } from "ts-essentials";
import tsDefaults, { ObjectDefaults, WithEnforcedDefaults } from "ts-defaults";

import { IGameMap, IGameMapPlane } from "../../models/map";
import { carmackExpand } from "../../compression/carmack";
import { rlewExpand } from "../../compression/rlew";
import { WolfBuffer } from "../../buffer/WolfBuffer";
import { WolfFileReader } from "../shared/WolfFileReader";

export interface IGameMapsReaderOpts {
	mapNameLength?: number;
	numDataPointers?: number;
	numPlanes: number;
}

type EnforcedOpts = "mapNameLength" | "numDataPointers";
export const DEFAULT_GAME_MAPS_READER_OPTS: DeepReadonly<ObjectDefaults<IGameMapsReaderOpts, EnforcedOpts>> = {
	mapNameLength: 16,
	numDataPointers: 100,
};

export class GameMapsReader {
	static readonly MAX_TILE_VALUE = WolfBuffer.MAX_WORD;

	protected gmr: WolfFileReader;
	protected mapOffsets?: Map<number, number>;
	protected rlewTag?: number;
	protected opts: WithEnforcedDefaults<IGameMapsReaderOpts, EnforcedOpts>;

	constructor(private readonly mapHeadPath: PathLike, private readonly gameMapsPath: PathLike, opts: IGameMapsReaderOpts) {
		this.gmr = new WolfFileReader(gameMapsPath);
		this.opts = this.mergeDefaultOpts(opts);
	}

	protected mergeDefaultOpts = (opts: IGameMapsReaderOpts) =>
		tsDefaults(opts, DEFAULT_GAME_MAPS_READER_OPTS, ["mapNameLength", "numDataPointers"]);

	async cacheOffsets(
		force?: boolean,
		numDataPointers: number = DEFAULT_GAME_MAPS_READER_OPTS.numDataPointers
	): Promise<Map<number, number>> {
		if (this.mapOffsets && !force) {
			return this.mapOffsets;
		}

		this.mapOffsets = new Map<number, number>();

		const mhr = new WolfFileReader(this.mapHeadPath);
		// skip the RLEW tag (specifies the # of uncompressed bytes - MAPHEAD is not compressed for Wolf3D
		this.rlewTag = await mhr.readWord();

		// multi plane support

		// read in offsets
		const locations = await mhr.readDWords(numDataPointers);
		const { mapOffsets } = this;
		locations.forEach((location, i) => {
			if (location > 0) {
				mapOffsets.set(i, location);
			}
		});
		await mhr.close();

		return this.mapOffsets;
	}

	async length() {
		return (await this.cacheOffsets()).size;
	}

	async readAllMaps(): Promise<IGameMap[]> {
		const length = await this.length();
		const result: IGameMap[] = [];
		for (let i = 0; i < length; i++) {
			result.push(await this.readMap(i));
		}
		return result;
	}

	async readMap(mapNum: number, opts?: IGameMapsReaderOpts): Promise<IGameMap> {
		await this.cacheOffsets();

		const { gmr, mapOffsets, rlewTag } = this;
		if (!mapOffsets) {
			throw new Error(`Map offsets not read.`);
		}
		if (!rlewTag) {
			throw new Error(`rlewTag missing.`);
		}
		const { numPlanes, mapNameLength } = opts ? this.mergeDefaultOpts(opts) : this.opts;

		gmr.position = mapOffsets.get(mapNum) || -1;
		if (gmr.position === -1) {
			throw new Error(`Offset not found for mapNum ${mapNum}.`);
		}

		const buffer = WolfBuffer.alloc(
			Math.max(numPlanes * WolfBuffer.DWORD_SEGMENT_LENGTH, 2 * WolfBuffer.WORD_SEGMENT_LENGTH, mapNameLength)
		);
		const planeOffsets = await gmr.readDWordsWithBuffer(buffer, numPlanes);
		const planeCmpLengths = await gmr.readWordsWithBuffer(buffer, numPlanes);
		const [mapWidth, mapHeight] = await gmr.readWordsWithBuffer(buffer, 2);
		const mapName = await gmr.readStringWithBuffer(buffer, mapNameLength);

		const planes: IGameMapPlane[] = [];
		for (let i = 0; i < planeOffsets.length; i++) {
			const planeOffset = planeOffsets[i];
			const planeCmpLength = planeCmpLengths[i];
			gmr.position = planeOffset;

			const carmackCompressed = await gmr.readIntoBuffer(WolfBuffer.allocUnsafe(planeCmpLength), planeCmpLength);
			const carmackExpandedByteLength = carmackCompressed.readWord(0);
			const carmackExpanded = carmackExpand(carmackCompressed, carmackExpandedByteLength, carmackCompressed.position);
			if (carmackExpanded.byteLength !== carmackExpandedByteLength) {
				throw new Error(
					`Carmack expand failed. Expected uncompressed byte length ${carmackExpandedByteLength} but got ${carmackExpanded.byteLength}.`
				);
			}

			const rlewExpandedByteLength = carmackExpanded.readWord(0);
			const rlewExpanded = rlewExpand(
				carmackExpanded,
				carmackExpanded.byteLength,
				rlewExpandedByteLength,
				rlewTag,
				carmackExpanded.position
			);
			if (rlewExpanded.byteLength !== rlewExpandedByteLength) {
				throw new Error(
					`RLEW expand failed. Expected uncompressed byte length ${rlewExpandedByteLength} but got ${rlewExpanded.byteLength}.`
				);
			}

			const rawPlaneData = rlewExpanded.readSegments(
				rlewExpandedByteLength / WolfBuffer.WORD_SEGMENT_LENGTH,
				WolfBuffer.WORD_SEGMENT_LENGTH,
				0
			);
			const rawPlaneDataPartitioned: number[][] = [];
			for (let row = 0; row < mapHeight; row++) {
				const startIndex = mapWidth * row;
				rawPlaneDataPartitioned.push(rawPlaneData.slice(startIndex, startIndex + mapWidth));
			}

			planes.push({
				data: rawPlaneDataPartitioned,
				height: mapHeight,
				index: planes.length,
				width: mapWidth,
			});
		}

		return {
			height: mapHeight,
			index: mapNum,
			name: mapName,
			planes,
			width: mapWidth,
		};
	}

	async close(): Promise<void> {
		return this.gmr.close();
	}
}
