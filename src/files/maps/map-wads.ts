import * as path from "path";

import tsDefaults from "ts-defaults";

import { WadFileReader } from "../shared/WadFileReader";
import { readWdcMapFileFromBuffer, writeWdcMapFileToBuffer, IWriteWdcMapOpts, DEFAULT_WRITE_WDC_MAP_OPTS } from "../wdc/wdc-maps";
import { IGameMap, ECWolfMap } from "../../models/map";
import { WolfFileReader } from "../shared/WolfFileReader";
import { WadFileWriter } from "../shared/WadFileWriter";
import { WolfFileWriter } from "../shared/WolfFileWriter";
import { isValidLumpName, LUMP_NAME_ERROR_HINT } from "../shared/wads";

/**
 * @see http://www.shikadi.net/moddingwiki/WAD_Format
 */
export const readECWolfMapWad = async (filePath: string): Promise<ECWolfMap[]> => {
	const wfr = new WadFileReader(new WolfFileReader(filePath));

	const header = await wfr.readHeader();
	const fileEntries = await wfr.readLumpPointers(header);

	const planesLumpEntry = fileEntries.find((e) => e.lumpName === "PLANES");
	if (!planesLumpEntry) {
		throw new Error("Could not find PLANES lump.");
	}
	const { data } = await wfr.readEntireLump(planesLumpEntry);
	await wfr.close();

	const fileName = path.basename(filePath, path.extname(filePath));
	const gameMaps = readWdcMapFileFromBuffer(data);

	return gameMaps.map((gm) => ({ fileName, gameMap: gm }));
};

export interface IWriteECWolfMapWadOpts {
	internalMapName?: string;
	wdcOpts?: IWriteWdcMapOpts;
}

export const writeECWolfMapWad = async (filePath: string, map: IGameMap, opts?: IWriteECWolfMapWadOpts): Promise<string> => {
	const wfw = new WadFileWriter(new WolfFileWriter(filePath));

	const wadDir = wfw.begin("PWAD");
	const beginPosition = wfw.position;

	let internalMapName: string;
	if (opts?.internalMapName) {
		if (!isValidLumpName(opts.internalMapName)) {
			throw new Error(`${opts.internalMapName} is an invalid lump name. ${LUMP_NAME_ERROR_HINT}`);
		}
		internalMapName = opts.internalMapName;
	} else {
		internalMapName = path.basename(filePath.toUpperCase(), ".WAD").substr(0, 8);
	}
	wadDir.addPointer({ lumpName: internalMapName, lumpNumBytes: 0, lumpOffset: beginPosition });

	const wdcFormatOptions = tsDefaults(opts?.wdcOpts ?? {}, DEFAULT_WRITE_WDC_MAP_OPTS, ["maxMapNameBytes", "numPlanes"]);
	const wdcMapBuf = writeWdcMapFileToBuffer([map], wdcFormatOptions);
	const planesPtr = await wadDir.writeLump("PLANES", wdcMapBuf);

	wadDir.addPointer({ lumpName: "ENDMAP", lumpNumBytes: 0, lumpOffset: planesPtr.lumpOffset + planesPtr.lumpNumBytes });

	await wadDir.end(true);

	return path.resolve(process.cwd(), filePath);
};
