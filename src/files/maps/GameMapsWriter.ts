import { DeepReadonly } from "ts-essentials";
import tsDefaults, { ObjectDefaults, WithEnforcedDefaults } from "ts-defaults";
import { IGameMap } from "../../models/map";
import { fixedLengthArray } from "../../utils/collections";
import { WolfBuffer } from "../../buffer/WolfBuffer";
import { WolfFileWriter } from "../shared/WolfFileWriter";
import flatten from "lodash.flatten";
import { rlewCompress } from "../../compression/rlew";
import { carmackCompress } from "../../compression/carmack";

export interface IGameMapsWriterOpts {
	mapHeaderSuffix?: boolean; // include '!ID!'
	mapNameByteLength?: number;
	maxMaps?: number;
	numPlanes: number;
	rlewTag?: number;
}

type DefaultEnforcedOptKeys = "mapNameByteLength" | "maxMaps" | "rlewTag";
export const DEFAULT_GAME_MAPS_WRITER_OPTS: DeepReadonly<ObjectDefaults<IGameMapsWriterOpts, DefaultEnforcedOptKeys>> = {
	mapNameByteLength: 16,
	maxMaps: 100,
	rlewTag: 0xabcd,
};

export const MYSTERIOUS_MAP_HEADER_SUFFIX = "!ID!";
export const MAP_HEADER_MIN_BYTE_LENGTH = 38;
export const MAX_PLANES = 3;

interface IPlaneChunksData {
	planeOffsets: number[];
	planeByteLengths: number[];
}

type GameMapsFilePosition = number;

interface IGameMapMapHeaderBlockData extends IPlaneChunksData {
	mapWidth: number;
	mapHeight: number;
	mapName: string;
}

interface IChunkWriteResult {
	filePosition: number;
	byteLength: number;
}

/**
 * Based off of http://www.shikadi.net/moddingwiki/GameMaps_Format
 */
export class GameMapsWriter {
	static readonly MAX_TILE_VALUE = WolfBuffer.MAX_WORD;

	protected opts: WithEnforcedDefaults<IGameMapsWriterOpts, DefaultEnforcedOptKeys>;

	constructor(protected readonly mapHeadPath: string, protected readonly gameMapsPath: string, opts: IGameMapsWriterOpts) {
		this.opts = this.mergeDefaultOpts(opts);
	}

	protected mergeDefaultOpts(opts: IGameMapsWriterOpts) {
		return tsDefaults(opts, DEFAULT_GAME_MAPS_WRITER_OPTS, ["mapNameByteLength", "maxMaps", "rlewTag"]);
	}

	async writeGameMaps(maps: IGameMap[]): Promise<void> {
		const { gameMapsPath } = this;

		const gmw = new WolfFileWriter(gameMapsPath);
		await gmw.writeString("TED5v1.0");

		const gameMapsFilePositions: GameMapsFilePosition[] = [];
		for (let x = 0; x < maps.length; x++) {
			gameMapsFilePositions.push(await this.writeGameMap(gmw, maps[x]));
		}

		await Promise.all([gmw.close(), this.writeMapHead(gameMapsFilePositions)]);
	}

	protected async writeGameMap(gmw: WolfFileWriter, map: IGameMap): Promise<GameMapsFilePosition> {
		const { rlewTag } = this.opts;
		const { height: mapHeight, index: mapIndex, name: mapName, planes, width: mapWidth } = map;
		const numPlanes = Math.min(MAX_PLANES, map.planes.length);
		const planeLength = mapWidth * mapHeight;
		const planeByteLength = planeLength * 2; // each tile is 2 bytes
		const planeOffsets: number[] = [];
		const planeByteLengths: number[] = [];

		for (let plane = 0; plane < numPlanes; plane++) {
			const { data: planeData } = planes[plane];
			const flattenedPlaneData = flatten(planeData);
			if (flattenedPlaneData.length !== planeLength) {
				throw new Error(
					`Plane length mismatch (actual: ${flattenedPlaneData.length}; expected: ${planeLength}) for map ${mapIndex} plane ${plane}.`
				);
			}
			const rlewCompressed = rlewCompress(WolfBuffer.ofWords(flattenedPlaneData), planeByteLength, rlewTag);
			const rlewCompressedBlock = WolfBuffer.concat([WolfBuffer.ofWords([planeByteLength]), rlewCompressed]);
			const carmackCompressed = carmackCompress(rlewCompressedBlock, rlewCompressedBlock.byteLength);
			const carmackCompressedBlock = WolfBuffer.concat([WolfBuffer.ofWords([rlewCompressedBlock.byteLength]), carmackCompressed]);

			const planeOffset = gmw.position;
			planeOffsets.push(planeOffset);

			await gmw.writeBytesFromBuffer(carmackCompressedBlock, carmackCompressedBlock.byteLength);

			planeByteLengths.push(carmackCompressedBlock.byteLength);
		}

		const { filePosition } = await this.writeGameMapHeaderBlock(gmw, {
			mapHeight,
			mapName,
			mapWidth,
			planeOffsets,
			planeByteLengths,
		});

		return filePosition;
	}

	protected async writeGameMapHeaderBlock(gmw: WolfFileWriter, header: IGameMapMapHeaderBlockData): Promise<IChunkWriteResult> {
		const { mapHeaderSuffix, mapNameByteLength } = this.opts;
		const { position: filePosition } = gmw;
		let byteLength = MAP_HEADER_MIN_BYTE_LENGTH;

		let headerSuffixBuffer: Buffer | undefined;
		if (mapHeaderSuffix) {
			headerSuffixBuffer = Buffer.from(MYSTERIOUS_MAP_HEADER_SUFFIX, "ascii");
			byteLength += headerSuffixBuffer.byteLength;
		}

		const planeOffsets = fixedLengthArray(header.planeOffsets, MAX_PLANES, 0);
		const planeByteLengths = fixedLengthArray(header.planeByteLengths, MAX_PLANES, 0);

		const headerBuffer = new WolfBuffer(Buffer.alloc(byteLength, 0x00));
		headerBuffer.writeDWords(planeOffsets);
		headerBuffer.writeWords([...planeByteLengths, header.mapWidth, header.mapHeight]);
		headerBuffer.writeString(header.mapName, { fixedByteLength: mapNameByteLength });
		if (headerSuffixBuffer) {
			headerSuffixBuffer.copy(headerBuffer.buffer, headerBuffer.position);
		}

		await gmw.writeBytesFromBuffer(headerBuffer, headerBuffer.byteLength);

		return { filePosition, byteLength };
	}

	protected async writeMapHead(gameMapsFilePositions: readonly GameMapsFilePosition[]) {
		const { mapHeadPath, opts } = this;
		const { maxMaps, rlewTag } = opts;

		const positions = fixedLengthArray(gameMapsFilePositions, maxMaps, 0);

		const mhw = new WolfFileWriter(mapHeadPath);
		await mhw.writeWord(rlewTag);
		await mhw.writeDWords(positions);
		await mhw.close();
	}
}
