import * as path from "path";

import * as fs from "fs-extra";
import nearley from "nearley";
import GRAMMAR_MAPINFO from "../../grammar/parsers/mapinfo-parser";

export const newMapInfoParser = () => new nearley.Parser(nearley.Grammar.fromCompiled(GRAMMAR_MAPINFO));

export const readMapInfoFile = async (mapInfoPath: string, parser: nearley.Parser = newMapInfoParser()) =>
	parser.feed(await fs.readFile(mapInfoPath, "utf8")).results;

const deepReadMapInfoRecurse = async (
	nodesAcc: any[],
	mapInfoPath: string,
	includeResolvePath: string,
	includes: string[]
): Promise<any[]> => {
	// read current file
	const selfIncludePath = path.relative(includeResolvePath, mapInfoPath);
	if (includes.includes(selfIncludePath)) {
		return nodesAcc;
	}
	includes.push(selfIncludePath);
	let nodes: any[];
	try {
		nodes = (await readMapInfoFile(mapInfoPath, newMapInfoParser()))[0];
	} catch (e) {
		console.error(`Failed to parse mapinfo file ${mapInfoPath}.`);
		throw e;
	}

	// find includes and accumulate nodes
	const includePaths: string[] = [];
	for (const node of nodes) {
		if (!node) {
			continue;
		}
		if (node.type === "include") {
			const includePath = path.join(includeResolvePath, node.path.value);
			if (!includePaths.includes(includePath)) {
				includePaths.push(includePath);
			}
		} else {
			nodesAcc.unshift(node);
		}
	}

	// read each included file (depth-first)
	for (const includePath of includePaths) {
		await deepReadMapInfoRecurse(nodesAcc, includePath, includeResolvePath, includes);
	}

	return nodesAcc;
};

export const deepReadMapInfoFile = async (mapInfoPath: string, includeResolvePath: string) =>
	deepReadMapInfoRecurse([], mapInfoPath, includeResolvePath, []);
