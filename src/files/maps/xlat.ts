import * as fs from "fs-extra";
import nearley from "nearley";
import GRAMMAR_XLAT from "../../grammar/parsers/xlat-parser";

const newXlatParser = () => new nearley.Parser(nearley.Grammar.fromCompiled(GRAMMAR_XLAT));

export const readXlatFile = async (filePath: string | Buffer | number, parser: nearley.Parser = newXlatParser()) =>
	parser.feed(await fs.readFile(filePath, "utf8")).results;
