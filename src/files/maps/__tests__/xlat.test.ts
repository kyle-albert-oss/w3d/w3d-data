import * as path from "path";
import { readXlatFile } from "../xlat";

const w3dXlatPath = path.resolve(process.cwd(), "assets/lzwolf/exploded/xlat/wolf3d.txt");

let result: any[];

beforeAll(async () => {
	result = await readXlatFile(w3dXlatPath);
	expect(result.length).toEqual(1); // > 1 means ambiguous
	result = result[0];
});

it("Recognizes tiles block", () => {
	expect(result[0].type).toEqual("block");
	expect(result[0].header?.type?.value).toEqual("tiles");
});
