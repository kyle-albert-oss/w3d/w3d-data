import * as fs from "fs-extra";
import * as path from "path";
import { MAP_HEADER_MIN_BYTE_LENGTH, MYSTERIOUS_MAP_HEADER_SUFFIX, GameMapsWriter } from "../GameMapsWriter";

import { IGameMap } from "../../../models/map";
import { GameMapsReader } from "../GameMapsReader";
import cloneDeep from "lodash.clonedeep";
import { isCIEnv } from "../../../utils/tests";

const gmSourcePath = path.resolve(process.cwd(), "assets/wl6/GAMEMAPS.WL6");
const mhSourcePath = path.resolve(process.cwd(), "assets/wl6/MAPHEAD.WL6");
const GM_TARGET_PATH = path.resolve(process.cwd(), "assets/output/GAMEMAPS.WL6");
const MH_TARGET_PATH = path.resolve(process.cwd(), "assets/output/MAPHEAD.WL6");

describe("Constants", () => {
	it("min header byte length is 38", () => expect(MAP_HEADER_MIN_BYTE_LENGTH).toEqual(38));
	it("optional map header suffix is !ID!", () => expect(MYSTERIOUS_MAP_HEADER_SUFFIX).toEqual("!ID!"));
});

// TODO: reenable when this can be parallelized with thread.js
describe("Read and write", () => {
	let maps: IGameMap[];

	const modE1M1 = (map: IGameMap) => {
		const p0 = map.planes[0].data;
		expect(p0[1][1]).toEqual(1);
		expect(p0[2][1]).toEqual(1);
		expect(p0[1][2]).toEqual(1);
		expect(p0[2][2]).toEqual(1);

		p0[1][1] = 17;
		p0[1][2] = 17;
		p0[2][1] = 17;
		p0[2][2] = 17;
		p0[38][34] = 92;
	};

	const testE1M1 = (map: IGameMap) => {
		// plane 0 test
		let d = map.planes[0].data;
		expect(d[0][0]).toEqual(1);
		expect(d[1][1]).toEqual(17);
		expect(d[2][1]).toEqual(17);
		expect(d[1][2]).toEqual(17);
		expect(d[2][2]).toEqual(17);
		expect(d[21][13]).toEqual(6);
		expect(d[38][34]).toEqual(92);
		expect(d[42][52]).toEqual(118);
		expect(d[22][30]).toEqual(10);

		// plane 1 tests
		d = map.planes[1].data;
		expect(d[0][0]).toEqual(0);
		expect(d[19][10]).toEqual(0);
		expect(d[44][52]).toEqual(97);
		expect(d[33][29]).toEqual(27);
		expect(d[57][31]).toEqual(124);

		// plane 2 tests

		d = map.planes[2].data;
		for (let y = 0; y < 64; y++) {
			for (let x = 0; x < 64; x++) {
				try {
					expect(d[y][x]).toEqual(0);
				} catch (e) {
					fail(`Expected [${x}, ${y}](x, y) to equal 0 but got ${d[y][x]}.`);
				}
			}
		}
	};

	beforeAll(
		async () => {
			[GM_TARGET_PATH, MH_TARGET_PATH].forEach((p) => {
				try {
					fs.unlinkSync(p);
				} catch (e) {
					console.warn(e);
				}
			});

			const readStart = Date.now();
			maps = await new GameMapsReader(mhSourcePath, gmSourcePath, {
				numPlanes: 3,
			}).readAllMaps();
			console.log(`Time taken to read maps (ms): `, Date.now() - readStart);
		},
		isCIEnv() ? 20000 : undefined
	);

	it(
		"Reads and writes E1M1",
		async () => {
			const newMap = cloneDeep(maps[0]);
			modE1M1(newMap);

			const writeStart = Date.now();
			const gmw = new GameMapsWriter(MH_TARGET_PATH, GM_TARGET_PATH, {
				mapHeaderSuffix: true,
				numPlanes: 3,
			});
			await gmw.writeGameMaps([newMap, ...maps.slice(1)]);
			console.log(`Time taken to write maps (ms): `, Date.now() - writeStart);

			// reread
			const gmr2 = new GameMapsReader(MH_TARGET_PATH, GM_TARGET_PATH, {
				numPlanes: 3,
			});

			const e1m1 = await gmr2.readMap(0, {
				numPlanes: 3,
			});

			testE1M1(e1m1);
		},
		isCIEnv() ? 60000 : 30000
	);
});
