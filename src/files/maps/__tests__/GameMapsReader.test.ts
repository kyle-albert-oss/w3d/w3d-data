import * as path from "path";
import { IGameMap } from "../../../models/map";

import { GameMapsReader } from "../GameMapsReader";

const gm1Path = path.resolve(process.cwd(), "assets/wl6/GAMEMAPS.WL6");
const mh1Path = path.resolve(process.cwd(), "assets/wl6/MAPHEAD.WL6");

// NOTE: plane coords reminder: data[y][x]

describe("E1L1", () => {
	let map: IGameMap;

	beforeAll(async () => {
		const gmr = new GameMapsReader(mh1Path, gm1Path, {
			numPlanes: 3,
		});

		map = await gmr.readMap(0);
		await gmr.close();
	});

	it("reads map name", () => expect(map.name).toEqual("Wolf1 Map1"));
	it("reads width and height", () => expect(map.width).toEqual(64));
	it("reads in 3 planes", () => expect(map.planes.length).toEqual(3));
	it("sets index as it is ordered by the MAPHEAD", () => expect(map.index).toEqual(0));

	it("reads plane 0", () => {
		const p = map.planes[0];

		expect(p.index).toEqual(0);
		expect(p.width).toEqual(64);
		expect(p.height).toEqual(64);

		const d = p.data;

		expect(d[63][63]).toEqual(1);
		expect(d[0][0]).toEqual(1);
		expect(d[21][13]).toEqual(6);
		expect(d[38][34]).toEqual(91);
		expect(d[42][52]).toEqual(118);
		expect(d[22][30]).toEqual(10);
		expect(d[63][63]).toEqual(1);
	});

	it("reads plane 1", () => {
		const p = map.planes[1];

		expect(p.index).toEqual(1);
		expect(p.width).toEqual(64);
		expect(p.height).toEqual(64);

		const d = p.data;

		expect(d[0][0]).toEqual(0);
		expect(d[19][10]).toEqual(0);
		expect(d[44][52]).toEqual(97);
		expect(d[33][29]).toEqual(27);
		expect(d[57][31]).toEqual(124);
		expect(d[63][63]).toEqual(0);
	});

	it("reads plane 2", () => {
		const p = map.planes[2];

		expect(p.index).toEqual(2);
		expect(p.width).toEqual(64);
		expect(p.height).toEqual(64);

		const d = p.data;
		expect(d.length).toEqual(64);

		for (let y = 0; y < 64; y++) {
			expect(d[y].length).toEqual(64);

			for (let x = 0; x < 64; x++) {
				try {
					expect(d[y][x]).toEqual(0);
				} catch (e) {
					fail(`Expected [${x}, ${y}](x, y) to equal 0 but is ${d[y][x]}.`);
				}
			}
		}
	});
});

describe("E2L8", () => {
	let map: IGameMap;

	beforeAll(async () => {
		const gmr = new GameMapsReader(mh1Path, gm1Path, {
			numPlanes: 3,
		});

		map = await gmr.readMap(17);
		await gmr.close();
	});

	it("reads map name", () => expect(map.name).toEqual("Wolf2 Map8"));
	it("reads width and height", () => expect(map.width).toEqual(64));
	it("reads in 3 planes", () => expect(map.planes.length).toEqual(3));
	it("sets index as it is ordered by the MAPHEAD", () => expect(map.index).toEqual(17));

	it("reads plane 0", () => {
		const p = map.planes[0];

		expect(p.index).toEqual(0);
		expect(p.width).toEqual(64);
		expect(p.height).toEqual(64);

		const d = p.data;

		expect(d[0][0]).toEqual(1);
		expect(d[29][36]).toEqual(3);
		expect(d[30][37]).toEqual(13);
		expect(d[33][55]).toEqual(94);
		expect(d[42][48]).toEqual(109);
	});

	it("reads plane 1", () => {
		const p = map.planes[1];

		expect(p.index).toEqual(1);
		expect(p.width).toEqual(64);
		expect(p.height).toEqual(64);

		const d = p.data;

		expect(d[0][0]).toEqual(0);
		expect(d[22][13]).toEqual(56);
		expect(d[30][38]).toEqual(20);
		expect(d[24][57]).toEqual(48);
		expect(d[47][32]).toEqual(98);
	});
});
