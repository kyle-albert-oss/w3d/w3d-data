import * as fs from "fs";
import * as path from "path";

import { readECWolfMapWad, writeECWolfMapWad } from "../map-wads";
import { isCIEnv } from "../../../utils/tests";
import { IGameMap } from "../../../models/map";

const WAD_1_PATH = path.resolve(process.cwd(), "assets/maps/wolf1map1.wad");

const testWolf1Map1 = (map: IGameMap) => {
	const { planes } = map;
	expect(planes.length).toEqual(3);
	expect(planes[0].width).toEqual(64);
	expect(planes[0].height).toEqual(64);
	expect(planes[1].width).toEqual(64);
	expect(planes[1].height).toEqual(64);
	expect(planes[2].width).toEqual(64);
	expect(planes[2].height).toEqual(64);

	let d = planes[0].data;

	expect(d[63][63]).toEqual(1);
	expect(d[0][0]).toEqual(1);
	expect(d[21][13]).toEqual(6);
	expect(d[38][34]).toEqual(91);
	expect(d[42][52]).toEqual(118);
	expect(d[22][30]).toEqual(10);
	expect(d[63][63]).toEqual(1);

	d = planes[1].data;

	expect(d[0][0]).toEqual(0);
	expect(d[19][10]).toEqual(0);
	expect(d[44][52]).toEqual(97);
	expect(d[33][29]).toEqual(27);
	expect(d[57][31]).toEqual(124);
};

describe("ECWolf map wads", () => {
	describe("Read", () => {
		it("Reads an EC map wad file correctly.", async () => {
			const maps = await readECWolfMapWad(WAD_1_PATH);

			expect(maps.length).toEqual(1);

			testWolf1Map1(maps[0].gameMap);
		});
	});

	describe("Write", () => {
		let maps: IGameMap[];

		beforeAll(
			async () => {
				maps = (await readECWolfMapWad(WAD_1_PATH)).map((ec) => ec.gameMap);
				expect(maps.length).toEqual(1);
			},
			isCIEnv() ? 10000 : undefined
		);

		it(
			"Writes an EC map wad file correctly.",
			async () => {
				const inputPath = "assets/output/mapw.wad";
				if (fs.existsSync(inputPath)) {
					fs.unlinkSync(inputPath);
				}
				const resultPath = await writeECWolfMapWad("assets/output/mapw.wad", maps[0]);
				const rereadMaps = await readECWolfMapWad(resultPath);

				expect(rereadMaps.length).toEqual(1);
				testWolf1Map1(rereadMaps[0].gameMap);
			},
			isCIEnv() ? 30000 : undefined
		);
	});
});
