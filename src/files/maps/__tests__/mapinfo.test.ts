import { readMapInfoFile, deepReadMapInfoFile } from "../mapinfo";
import * as path from "path";

const nodeOfType = (t: string) => (n: any) => n != null && typeof n === "object" && n.type === t;

describe("wolf3d.txt", () => {
	let result: any[];
	const w3dMapInfoPath = path.resolve(process.cwd(), "assets/lzwolf/exploded/mapinfo/wolf3d.txt");

	beforeAll(async () => {
		result = await readMapInfoFile(w3dMapInfoPath);
		expect(result.length).toEqual(1); // > 1 means ambiguous
		result = result[0];
	});

	it("Recognizes includes", () => {
		const node = result[0];
		expect(node.type).toEqual("include");
		expect(node.path.value).toEqual("mapinfo/wolfcommon.txt");
	});

	it("Recognizes gameinfo block", () => {
		const node = result.filter(nodeOfType("block")).find((n) => n.header?.type?.value === "gameinfo");

		expect(node.type).toEqual("block");
		expect(node.header?.type?.value).toEqual("gameinfo");
		expect(node.body.length).toEqual(12);
	});
});

describe("danger.txt", () => {
	let result: any[];
	const dangerMapInfoPath = path.resolve(process.cwd(), "assets/lzwolf/exploded/mapinfo/danger.txt");

	beforeAll(async () => {
		result = await deepReadMapInfoFile(dangerMapInfoPath, path.resolve(process.cwd(), "assets/lzwolf/exploded"));
	});

	it("Has no includes", () => {
		expect(result.filter(nodeOfType("include")).length).toEqual(0);
	});

	it("Recognizes gameinfo block", () => {
		const nodes = result.filter(nodeOfType("block")).filter((n) => n.header?.type?.value === "gameinfo");

		expect(nodes.length).toEqual(3);
	});
});
