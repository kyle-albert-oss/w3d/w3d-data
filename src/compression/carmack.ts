import { WolfBuffer } from "../buffer/WolfBuffer";

export enum CarmackFlag {
	NEAR = 0xa7,
	FAR = 0xa8,
}

export const carmackCompress = (buffer: WolfBuffer, numBytes: number, srcIdx: number = 0): WolfBuffer => {
	let lengthInWords = Math.floor((numBytes + 1) / WolfBuffer.WORD_SEGMENT_LENGTH);
	let maxString;
	let stringIdx;
	let bestStringIdx;
	let bestScanIdx: number = 0;

	const result = WolfBuffer.alloc(numBytes);

	while (lengthInWords) {
		const value = buffer.readWord(srcIdx * WolfBuffer.WORD_SEGMENT_LENGTH);
		bestStringIdx = 0;
		for (let srcScanIdx = 0; srcScanIdx < srcIdx; srcScanIdx++) {
			if (value !== buffer.readWord(srcScanIdx * WolfBuffer.WORD_SEGMENT_LENGTH)) {
				continue;
			}

			maxString = srcIdx - srcScanIdx;
			maxString = Math.min(255, lengthInWords, maxString);

			stringIdx = 1;
			while (
				stringIdx < maxString &&
				buffer.readWord((srcScanIdx + stringIdx) * WolfBuffer.WORD_SEGMENT_LENGTH) ===
					buffer.readWord((srcIdx + stringIdx) * WolfBuffer.WORD_SEGMENT_LENGTH)
			) {
				stringIdx++;
			}

			if (stringIdx >= bestStringIdx) {
				bestStringIdx = stringIdx;
				bestScanIdx = srcScanIdx;
			}
		}

		if (bestStringIdx > 1 && srcIdx - bestScanIdx <= 255) {
			result.writeBytes([bestStringIdx, CarmackFlag.NEAR, srcIdx - bestScanIdx]);
			srcIdx += bestStringIdx;
			lengthInWords -= bestStringIdx;
		} else if (bestStringIdx > 2) {
			result.writeBytes([bestStringIdx, CarmackFlag.FAR]);
			result.writeWord(bestScanIdx);
			srcIdx += bestStringIdx;
			lengthInWords -= bestStringIdx;
		} else {
			const valueHigh = Math.floor(value >> 8);
			if (valueHigh === CarmackFlag.NEAR || valueHigh === CarmackFlag.FAR) {
				result.writeWord(value & 0xff00);
				result.writeByte(value & 0xff);
			} else {
				result.writeWord(value);
			}
			srcIdx++;
			lengthInWords--;
		}

		if (lengthInWords < 0) {
			throw new Error(`end < 0!`);
		}
	}

	return WolfBuffer.from(result.buffer, 0, result.position);
};

export const carmackExpand = (buffer: WolfBuffer, numBytes: number, srcIdx: number = 0): WolfBuffer => {
	let ch, chhigh, count, offset;
	let index = 0;
	let lengthInWords = Math.floor((numBytes + 1) / WolfBuffer.WORD_SEGMENT_LENGTH);
	const expandedWords: number[] = [];

	buffer.position = srcIdx;

	while (lengthInWords > 0) {
		ch = buffer.readWord();
		chhigh = ch >> 8;

		if (chhigh === CarmackFlag.NEAR) {
			count = ch & 0xff;

			if (count === 0) {
				ch |= buffer.readByte();
				expandedWords[index++] = ch;
				lengthInWords--;
			} else {
				offset = buffer.readByte();
				lengthInWords -= count;
				if (lengthInWords < 0) {
					return WolfBuffer.ofWords(expandedWords);
				}
				while (count-- > 0) {
					expandedWords[index] = expandedWords[index - offset];
					index++;
				}
			}
		} else if (chhigh === CarmackFlag.FAR) {
			count = ch & 0xff;

			if (count === 0) {
				ch |= buffer.readByte();
				expandedWords[index++] = ch;
				lengthInWords--;
			} else {
				offset = buffer.readWord();
				lengthInWords -= count;
				if (lengthInWords < 0) {
					return WolfBuffer.ofWords(expandedWords);
				}
				while (count-- > 0) {
					expandedWords[index++] = expandedWords[offset++];
				}
			}
		} else {
			expandedWords[index++] = ch;
			lengthInWords--;
		}
	}

	return WolfBuffer.ofWords(expandedWords);
};
