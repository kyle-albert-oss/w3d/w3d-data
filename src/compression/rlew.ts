import { WolfBuffer } from "../buffer/WolfBuffer";

export const rlewCompress = (buffer: WolfBuffer, numBytes: number, rlewTag: number, srcIndex: number = 0): WolfBuffer => {
	const result = WolfBuffer.alloc(numBytes);
	const lastWordPos = numBytes - WolfBuffer.WORD_SEGMENT_LENGTH;

	while (srcIndex <= lastWordPos) {
		let count = 1;
		const value = buffer.readWord(srcIndex);
		srcIndex = buffer.position;
		while (srcIndex <= lastWordPos && buffer.readWord(srcIndex) === value) {
			count++;
			srcIndex = buffer.position;
		}

		if (count > 3 || value === rlewTag) {
			result.writeWords([rlewTag, count, value]);
		} else {
			for (let i = 1; i <= count; i++) {
				result.writeWord(value);
			}
		}
	}

	return WolfBuffer.from(result.buffer, 0, result.position);
};

export const rlewExpand = (
	buffer: WolfBuffer,
	numBytes: number,
	expandedByteLength: number,
	rlewTag: number,
	srcIndex: number = 0
): WolfBuffer => {
	const result = WolfBuffer.allocUnsafe(expandedByteLength);

	let value, count;
	const lastWordPos = numBytes - WolfBuffer.WORD_SEGMENT_LENGTH;
	buffer.position = srcIndex;

	while (buffer.position <= lastWordPos) {
		value = buffer.readWord();
		if (value !== rlewTag) {
			result.writeWord(value);
		} else {
			[count, value] = buffer.readWords(2);
			for (let i = 1; i <= count; i++) {
				result.writeWord(value);
			}
		}
	}

	return result;
};
