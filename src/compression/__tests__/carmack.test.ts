import { carmackCompress } from "../carmack";

import COMPRESSED_JSON from "./carmack-compressed.json";
import EXPANDED_JSON from "./carmack-expanded.json";
import { WolfBuffer } from "../../buffer/WolfBuffer";

const COMPRESSED_1 = COMPRESSED_JSON;
const EXPANDED_1 = EXPANDED_JSON;

describe("Compress", () => {
	it("Correctly compresses a byte buffer.", () => {
		const result = carmackCompress(WolfBuffer.ofBytes(EXPANDED_1), EXPANDED_1.length);
		expect([...result]).toEqual(COMPRESSED_1);
	});
});

// describe("Expand", () => {
// 	it("Correctly expands a byte buffer.", () => {
// 		const result = carmackExpand(WolfBuffer.ofBytes(COMPRESSED_1), COMPRESSED_1.length);
// 		expect([...result]).toEqual(EXPANDED_1);
// 	});
// });
