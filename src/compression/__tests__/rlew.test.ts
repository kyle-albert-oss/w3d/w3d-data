import { WolfBuffer } from "../../buffer/WolfBuffer";
import { rlewExpand, rlewCompress } from "../rlew";

const RLEW_TAG_1 = 0xabcd;
const COMPRESSED_1_BYTE_LENGTH = 38;
const COMPRESSED_1 = [
	0x0234,
	0xffff,
	0xabcd,
	0x000f,
	0x1234, // 15
	0x1212,
	0xabcd,
	0x0001,
	0xabcd, // 1
	0x2222,
	0x3334,
	0xb2ef,
	0xf212,
	0xabcd,
	0x0004,
	0xcccc, // 4
	0x890f,
	0xaaaa,
	0xabed,
];
const EXPANDED_1_BYTE_LENGTH = 30 * WolfBuffer.WORD_SEGMENT_LENGTH;
const EXPANDED_1 = [
	0x0234,
	0xffff,
	0x1234,
	0x1234,
	0x1234,
	0x1234,
	0x1234,
	0x1234,
	0x1234,
	0x1234,
	0x1234,
	0x1234,
	0x1234,
	0x1234,
	0x1234,
	0x1234,
	0x1234,
	0x1212,
	0xabcd,
	0x2222,
	0x3334,
	0xb2ef,
	0xf212,
	0xcccc,
	0xcccc,
	0xcccc,
	0xcccc,
	0x890f,
	0xaaaa,
	0xabed,
];

describe("Expand", () => {
	it("Correctly expands a simple word buffer.", () => {
		const src = WolfBuffer.ofWords(COMPRESSED_1);

		const result = rlewExpand(src, src.byteLength, EXPANDED_1_BYTE_LENGTH, RLEW_TAG_1);
		result.position = 0;

		expect(result.readWords(EXPANDED_1_BYTE_LENGTH / WolfBuffer.WORD_SEGMENT_LENGTH)).toEqual(EXPANDED_1);
	});
});

describe("Compress", () => {
	it("Correctly compresses simple word buffer.", () => {
		const src = WolfBuffer.ofWords(EXPANDED_1);

		const result = rlewCompress(src, src.byteLength, RLEW_TAG_1);

		expect(result.readWords(COMPRESSED_1_BYTE_LENGTH / WolfBuffer.WORD_SEGMENT_LENGTH)).toEqual(COMPRESSED_1);
	});
});
