import nearley from "nearley";

export const W3D_MAX_PALETTE_COLORS = 256;

export type RGBTuple = [number, number, number];
export type PaletteColor = RGBTuple;

export interface IPaletteFileReaderFn {
	(filePath: string | Buffer | number, parser?: nearley.Parser): Promise<PaletteColor[]>;
}
