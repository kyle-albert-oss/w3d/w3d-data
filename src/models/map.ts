export interface IGameMap {
	height: number;
	index: number;
	name: string;
	planes: IGameMapPlane[];
	width: number;
}

export interface IGameMapPlane {
	data: number[][];
	height: number;
	index: number;
	width: number;
}

export interface IECWolfMapWad {
	fileName: string;
	gameMap: IGameMap;
}

export type ECWolfMap = IECWolfMapWad; // TODO: uwmf
