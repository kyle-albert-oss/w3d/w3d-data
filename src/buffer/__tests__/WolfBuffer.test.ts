import { WolfBuffer } from "../WolfBuffer";

describe("WolfBuffer", () => {
	describe("WolfBuffer.from", () => {
		it("Uses the same buffer if offset is 0 and byte length is equal.", () => {
			const src = Buffer.from([0x07, 0x05, 0x03, 0x01, 0x50, 0x22]);
			const result = WolfBuffer.from(src);

			expect(result.buffer).toBe(src);
			expect([...result]).toEqual([0x07, 0x05, 0x03, 0x01, 0x50, 0x22]);
			expect(result.position).toEqual(0);
		});

		it("Copies the buffer correctly when given an offset.", () => {
			const src = Buffer.from([0x07, 0x05, 0x03, 0x01, 0x50, 0x22]);
			const result = WolfBuffer.from(src, 1);

			expect(result.buffer).not.toBe(src);
			expect([...result]).toEqual([0x05, 0x03, 0x01, 0x50, 0x22]);
			expect(result.position).toEqual(0);
		});

		it("Copies the buffer correctly when given an offset and length.", () => {
			const src = Buffer.from([0x07, 0x05, 0x03, 0x01, 0x50, 0x22]);
			const result = WolfBuffer.from(src, 2, 2);

			expect(result.buffer).not.toBe(src);
			expect([...result]).toEqual([0x03, 0x01]);
			expect(result.position).toEqual(0);
		});

		it("Handles out of range offset.", () => {
			const src = Buffer.from([0x07, 0x05, 0x03, 0x01, 0x50, 0x22]);
			const result = WolfBuffer.from(src, 6);

			expect([...result]).toEqual([]);
			expect(result.position).toEqual(0);
		});

		it("Handles out of range length.", () => {
			const src = Buffer.from([0x07, 0x05, 0x03, 0x01, 0x50, 0x22]);
			const result = WolfBuffer.from(src, 3, 4);

			expect([...result]).toEqual([0x01, 0x50, 0x22]);
			expect(result.position).toEqual(0);
		});
	});

	describe("WolfBuffer.ofWords", () => {
		it("Initializes from word array correctly.", () => {
			const result = WolfBuffer.ofWords([0xfefd, 0x0000, 0x1234]);

			expect([...result]).toEqual([0xfd, 0xfe, 0x00, 0x00, 0x34, 0x12]);
			expect(result.position).toEqual(0);
		});

		it("Handles out of range numbers.", () => {
			try {
				WolfBuffer.ofWords([0xdffff]);
				fail("Did not throw.");
			} catch (e) {
				expect(e).toBeTruthy();
			}
		});
	});

	describe("WolfBuffer.ofBytes", () => {
		it("Initializes from byte array correctly.", () => {
			const result = WolfBuffer.ofBytes([0xfd, 0x00, 0x34]);

			expect([...result]).toEqual([0xfd, 0x00, 0x34]);
			expect(result.position).toEqual(0);
		});

		it("Handles out of range numbers.", () => {
			try {
				WolfBuffer.ofBytes([0x1ff]);
				fail("Did not throw.");
			} catch (e) {
				expect(e).toBeTruthy();
			}
		});
	});

	describe("readByte(s)", () => {
		it("Reads a byte and increments position.", () => {
			const src = WolfBuffer.ofWords([0xabcd, 0x1234]);

			let byte = src.readByte();
			expect(byte).toEqual(0xcd);
			expect(src.position).toEqual(1);

			byte = src.readByte();
			expect(byte).toEqual(0xab);
			expect(src.position).toEqual(2);
		});

		it("Reads a byte at specified position.", () => {
			const src = WolfBuffer.ofWords([0xabcd, 0x1234]);

			expect(src.readByte(2)).toEqual(0x34);
			expect(src.position).toEqual(3);
		});
	});

	describe("readWord(s)", () => {
		it("Reads a word and increments position.", () => {
			const src = WolfBuffer.ofWords([0xabcd, 0x1234]);

			let byte = src.readWord();
			expect(byte).toEqual(0xabcd);
			expect(src.position).toEqual(2);

			byte = src.readWord();
			expect(byte).toEqual(0x1234);
			expect(src.position).toEqual(4);
		});

		it("Reads a word at specified position.", () => {
			const src = WolfBuffer.ofWords([0xabcd, 0x1234]);

			expect(src.readWord(2)).toEqual(0x1234);
			expect(src.position).toEqual(4);
		});

		it("Handles max word.", () => {
			const underTest = WolfBuffer.ofWords([0xffff]);
			expect(underTest.readWord()).toEqual(WolfBuffer.MAX_WORD);
		});
	});

	describe("readDWord(s)", () => {
		it("Handles max dword.", () => {
			const underTest = WolfBuffer.allocUnsafe(WolfBuffer.DWORD_SEGMENT_LENGTH);
			underTest.writeDWord(0xffffffff);
			expect(underTest.readDWord(0)).toEqual(WolfBuffer.MAX_DWORD);
		});
	});

	describe("Strings", () => {
		it("Can write a string.", () => {
			const underTest = WolfBuffer.alloc(1000);

			underTest.writeString("AbC", { encoding: "ascii" });

			const result = underTest.readBytes(3, 0);
			expect(result).toEqual([0x41, 0x62, 0x43]);
		});

		it("Can read a string.", () => {
			const underTest = WolfBuffer.ofBytes([0x41, 0x62, 0x43]);

			const result = underTest.readString(3, { encoding: "ascii" });
			expect(result).toEqual("AbC");
		});
	});
});
