export interface IWolfBufferWriteStringOpts {
	encoding?: BufferEncoding;
	fixedByteLength?: number;
	nullTerminate?: "always";
}

export interface IWolfBufferReadStringOpts {
	encoding?: BufferEncoding;
}

/**
 * Wrapper on node buffer to help with reading old school file data.
 * Here, a "word" is 2 bytes; "dword" is 4 bytes
 * Keeping these terms to maintain some parity with Wolfenstein 3D source code.
 */
export class WolfBuffer {
	static readonly WORD_SEGMENT_LENGTH = 2;
	static readonly DWORD_SEGMENT_LENGTH = 4;
	static readonly MAX_WORD = 0xffff; // 65_535
	static readonly MAX_DWORD = 0xffffffff; // 4_294_967_295

	static fromArrayBuffer(buffer: ArrayBuffer | SharedArrayBuffer, offset: number = 0, numBytes?: number) {
		return new WolfBuffer(Buffer.from(buffer, offset, numBytes));
	}

	static from(buffer: Buffer, offset: number = 0, numBytes?: number) {
		numBytes = Math.min(numBytes ?? Number.POSITIVE_INFINITY, buffer.byteLength - offset);
		if (offset === 0 && numBytes === buffer.byteLength) {
			return new WolfBuffer(buffer);
		} else {
			const newBuf = Buffer.allocUnsafe(numBytes);
			buffer.copy(newBuf, 0, offset, offset + numBytes);
			return new WolfBuffer(newBuf);
		}
	}

	static alloc(numBytes: number, fill?: number | string | Buffer | undefined, encoding?: BufferEncoding) {
		return new WolfBuffer(Buffer.alloc(numBytes, fill, encoding));
	}

	static allocUnsafe(numBytes: number) {
		return new WolfBuffer(Buffer.allocUnsafe(numBytes));
	}

	static ofWords(wordArray: number[]) {
		const buf = WolfBuffer.allocUnsafe(wordArray.length * WolfBuffer.WORD_SEGMENT_LENGTH);
		buf.writeWords(wordArray);
		buf.position = 0;
		return buf;
	}

	static ofBytes(byteArray: number[]) {
		const buf = WolfBuffer.allocUnsafe(byteArray.length);
		buf.writeBytes(byteArray);
		buf.position = 0;
		return buf;
	}

	static concat(buffers: WolfBuffer[]) {
		return WolfBuffer.from(Buffer.concat(buffers.map((b) => b.buffer)));
	}

	position: number = 0; // for the sake of convenience without have a to track a position in outer context

	constructor(public readonly buffer: Buffer) {}

	[Symbol.iterator]() {
		return this.buffer.values();
	}

	get arrayBuffer() {
		return this.buffer.buffer;
	}

	get byteLength() {
		return this.buffer.byteLength;
	}

	/**
	 * Reads a segment from the buffer as an unsigned little-endian int.
	 * @param position position in file
	 * @param segmentLength segment length in bytes
	 */
	readSegment(segmentLength: number = 1, position: number = this.position): number {
		this.position = position + segmentLength;
		return this.buffer.readUIntLE(position, segmentLength);
	}

	readByte(position: number = this.position) {
		return this.readSegment(1, position);
	}

	readWord(position: number = this.position) {
		return this.readSegment(WolfBuffer.WORD_SEGMENT_LENGTH, position);
	}

	readDWord(position: number = this.position) {
		return this.readSegment(WolfBuffer.DWORD_SEGMENT_LENGTH, position);
	}

	readSegments(numSegments: number, segmentLength: number = 1, position: number = this.position): number[] {
		const arr: number[] = [];
		for (let x = 0; x < numSegments; x++) {
			arr.push(this.readSegment(segmentLength, position));
			position += segmentLength;
		}
		return arr;
	}

	readBytes(numBytes: number, position: number = this.position): number[] {
		return this.readSegments(numBytes, 1, position);
	}

	readWords(numWords: number, position: number = this.position): number[] {
		return this.readSegments(numWords, WolfBuffer.WORD_SEGMENT_LENGTH, position);
	}

	readDWords(numDWords: number, position: number = this.position): number[] {
		return this.readSegments(numDWords, WolfBuffer.DWORD_SEGMENT_LENGTH, position);
	}

	readString(length: number, opts: IWolfBufferReadStringOpts = {}, position: number = this.position): string {
		let result = this.buffer.toString(opts.encoding, position, position + length);
		const terminateIndex = result.indexOf("\u0000");
		if (terminateIndex !== -1) {
			result = result.slice(0, terminateIndex);
		}
		this.position += length;
		return result;
	}

	writeSegment(segment: number, segmentLength: number = 1, position: number = this.position) {
		this.position = position + segmentLength;
		return this.buffer.writeUIntLE(segment, position, segmentLength);
	}

	writeByte(byte: number, position: number = this.position) {
		return this.writeSegment(byte, 1, position);
	}

	writeWord(word: number, position: number = this.position) {
		return this.writeSegment(word, WolfBuffer.WORD_SEGMENT_LENGTH, position);
	}

	writeDWord(dword: number, position: number = this.position) {
		return this.writeSegment(dword, WolfBuffer.DWORD_SEGMENT_LENGTH, position);
	}

	writeSegments(segments: number[], segmentLength: number = 1, position: number = this.position): number[] {
		const arr: number[] = [];
		for (let x = 0; x < segments.length; x++) {
			arr.push(this.writeSegment(segments[x], segmentLength, position));
			position += segmentLength;
		}
		return arr;
	}

	writeBytes(bytes: number[], position: number = this.position): number[] {
		return this.writeSegments(bytes, 1, position);
	}

	writeWords(words: number[], position: number = this.position): number[] {
		return this.writeSegments(words, WolfBuffer.WORD_SEGMENT_LENGTH, position);
	}

	writeDWords(dwords: number[], position: number = this.position): number[] {
		return this.writeSegments(dwords, WolfBuffer.DWORD_SEGMENT_LENGTH, position);
	}

	/**
	 * Writes a string to the buffer.
	 *
	 * If the option fixedByteLength is set, the buffer is 0'ed out at the specified position all the way
	 * to position + fixedByteLength. The string is then written to the buffer. If the string byte length is
	 * longer than fixedByte length, it is truncated and only null-terminated in the last position if nullTerminate is "always".
	 *
	 * @param string the string to write
	 * @param opts options
	 * @param position position to start writing in buffer (default: current position marker)
	 */
	writeString(string: string, opts: IWolfBufferWriteStringOpts = {}, position: number = this.position) {
		let posIncrement = 0;
		const { encoding, fixedByteLength, nullTerminate } = opts;
		if (fixedByteLength != null) {
			const strBuf = Buffer.from(string, encoding);
			const strBufByteLength = strBuf.byteLength;
			this.buffer.fill(0x00, position, position + fixedByteLength);
			const maxEnd = nullTerminate ? fixedByteLength - 1 : fixedByteLength;
			strBuf.copy(this.buffer, position, 0, Math.min(strBufByteLength, maxEnd));
			posIncrement += maxEnd; // advance to the end of the string
		} else {
			posIncrement += this.buffer.write(string, position, encoding);
			this.position = position + posIncrement;
		}
		this.position = position + posIncrement;
		if (nullTerminate) {
			this.buffer.writeUIntLE(0x00, this.position, 1);
			this.position += 1;
		}
	}
}
