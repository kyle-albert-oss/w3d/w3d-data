/*
 * Test-only utils should go here. These will not be bundled and will not have TypeScript declaration files.
 */
export const isCIEnv = (): boolean => !!((process.env.CI && process.env.CI === "true") || (process.env.CI2 && process.env.CI2 === "true"));
