import * as path from "path";

export const getFileNameNoExt = (pathOrName: string): string => path.basename(pathOrName, path.extname(pathOrName));
