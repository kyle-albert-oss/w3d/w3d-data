export const fixedLengthArray = <T>(array: readonly T[], length: number, fill: T): T[] => {
	if (array.length === length) {
		return array as T[];
	} else if (array.length > length) {
		return array.slice(0, length);
	}
	const result = [...array];
	for (let x = length - array.length; x > 0; x--) {
		result.push(fill);
	}
	return result;
};
