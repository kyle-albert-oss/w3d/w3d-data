# wolf3d-data

Utilities for reading and writing ECWolf files and Wolfenstein 3D original game file formats.

## Requirements

- Node `>= 12`

## Installation

```shell script
npm i wolf3d-data
```

## Features

The following file formats can be read:

- [DECORATE](https://zdoom.org/wiki/DECORATE) **WIP**
  - Most ECWolf shipped decorate files are readable but there is still some grammar left to define and some bugs.
- [ECWolf map WAD](https://maniacsvault.net/ecwolf/wiki/Creating_Maps_And_Wads), see also [this link](https://maniacsvault.net/ecwolf/wiki/Data_Container_Formats)
- [GAMEMAPS](http://www.shikadi.net/moddingwiki/GameMaps_Format) (only WL6 variant tested)
- [JASC Palette](http://liero.nl/lierohack/docformats/other-jasc.html)
- [MAPINFO](https://maniacsvault.net/ecwolf/wiki/MAPINFO)
- [VSWAP](https://devinsmith.net/backups/bruce/wolf3d.html)
- [WDC](http://winwolf3d.dugtrio17.com/)
  - `map`: map data file that can be imported/exported from WDC
  - `wmc`: map definition
- [XLAT](https://maniacsvault.net/ecwolf/wiki/Map_translator)

The following file formats can be written:

- [ECWolf map WAD](https://maniacsvault.net/ecwolf/wiki/Creating_Maps_And_Wads), see also [this link](https://maniacsvault.net/ecwolf/wiki/Data_Container_Formats)
- [GAMEMAPS](http://www.shikadi.net/moddingwiki/GameMaps_Format) (only WL6 variant tested)
- [WDC](http://winwolf3d.dugtrio17.com/)
  - `map`: map data file that can be imported/exported from WDC

Compression algorithms:

- [Carmack](http://www.shikadi.net/moddingwiki/Carmack_compression)
- [RLEW](http://www.shikadi.net/moddingwiki/RLEW_compression)

## License

MIT
