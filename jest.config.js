/**
 * https://jestjs.io/docs/en/configuration
 */
module.exports = {
	globals: {
		"ts-jest": {
			tsConfig: "tsconfig.test.json",
		},
	},
	preset: "ts-jest",
	testEnvironment: "node", // "jsdom" for browser, "node" for node only package
	moduleFileExtensions: ["js", "jsx", "ts", "tsx"],
	modulePathIgnorePatterns: ["<rootDir>/coverage/", "<rootDir>/dist/", "<rootDir>/out/"],
	setupFilesAfterEnv: ["./tests/setup-tests.ts"],
	transform: {
		"\\.(tsx?)$": "ts-jest",
	},
};
