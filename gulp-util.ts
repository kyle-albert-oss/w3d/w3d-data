import chalk from "chalk";
// eslint-disable-next-line @typescript-eslint/camelcase
import * as childProcesses from "child_process";
import * as del from "del";
import log from "fancy-log";
import * as fs from "fs-extra";
import * as path from "path";
import * as gulp from "gulp";
import * as undertaker from "undertaker";
import { Extract } from "unzipper";

export type GulpTask = undertaker.Task;

const { task } = gulp;

export const successIcon = "✅";
export const warningIcon = "⚠️";
export const errorIcon = "❌";
export const deleteIcon = "💥";
export const runIcon = "👟";

export const logSuccess = (str: string) => log(successIcon, str);
export const logError = (err: string | Error) => log.error(errorIcon, err);
export const logWarn = (str: string) => log.warn(warningIcon, str);
export const logInfo = (str: string) => log.info(str);
export const logDelete = (str: string) => log.warn(deleteIcon, str);
export const logRun = (str: string) => log(runIcon, str);

export const inlineTask = (name: string, fn: gulp.TaskFunction) => {
	task(name, fn);
	return task(name);
};

export const inlineSyncTask = (name: string, fn: () => void) =>
	inlineTask(name, (cb) => {
		try {
			fn();
			cb();
		} catch (e) {
			cb(e);
		}
	});

export const unzipToFolderTask = (name: string, archivePath: string, destinationPath: string): GulpTask =>
	inlineTask(
		name,
		async () =>
			new Promise((r, rj) => {
				archivePath = path.resolve(process.cwd(), archivePath);
				destinationPath = path.resolve(process.cwd(), destinationPath);
				fs.mkdirpSync(destinationPath);
				fs.createReadStream(archivePath)
					.pipe(Extract({ path: destinationPath }))
					.on("error", rj)
					.on("close", () => {
						logSuccess(chalk`{green Unzipped {bold ${archivePath}} to {bold ${destinationPath}}}`);
						r(destinationPath);
					});
			})
	);

export const mkdirpTask = (name: string, dirPath: string | string[]) =>
	inlineSyncTask(name, () => {
		let paths: string[] = typeof dirPath === "string" ? [dirPath] : dirPath;
		for (const pth of paths) {
			const resolvedPath = path.resolve(process.cwd(), pth);
			fs.mkdirpSync(resolvedPath);
			logSuccess(chalk`{green Directory {bold ${resolvedPath}} exists.}`);
		}
	});

type DelSync = typeof del.sync;
export const rmTask = (name: string, fn: (d: DelSync) => ReturnType<DelSync>) =>
	inlineSyncTask(name, () => {
		// eslint-disable-next-line @typescript-eslint/unbound-method
		const result = fn(del.sync);
		if (result) {
			result.forEach((path) => logDelete(chalk`{yellow Deleted {bold ${path}}}`));
		}
	});

export const execCmd = async (cmd: string, opts?: childProcesses.SpawnOptions) =>
	new Promise((r, rj) => {
		logRun(chalk`{blue Executing: {bold.inverse ${cmd}}}`);
		const cmdProgram = cmd.trim().split(/\s+/)[0];
		const npmBinPath = path.resolve(process.cwd(), "node_modules/.bin");
		if (fs.existsSync(path.resolve(npmBinPath, cmdProgram))) {
			cmd = path.resolve(npmBinPath, cmd);
		}
		// seems to support dynamic stdio (loading bars, etc)
		childProcesses
			.spawn(cmd, { cwd: process.cwd(), stdio: "inherit", shell: true, ...opts, env: { ...process.env, ...opts?.env } })
			.on("error", (err) => {
				logError(err);
				rj(err);
			})
			.on("exit", (code) => (code != null && code > 0 ? rj(code) : r()));
	});

export const execTaskFn = (cmd: string, opts?: childProcesses.SpawnOptions): gulp.TaskFunction => async () => execCmd(cmd, opts);

export const execTask = (name: string, cmd: string, opts?: childProcesses.SpawnOptions) => inlineTask(name, execTaskFn(cmd, opts));
