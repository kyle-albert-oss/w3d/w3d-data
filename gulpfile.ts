import * as gulp from "gulp";
import { execTask, GulpTask, rmTask, mkdirpTask, inlineTask, unzipToFolderTask, execCmd, logSuccess } from "./gulp-util";
import chalk from "chalk";

const { dest, parallel, series, src, task } = gulp;

const srcDir = "src";
const cmdStr = (parts: (string | null | undefined)[]): string => parts.filter((p) => p != null && p !== "").join(" ");
const jestCmd = (opts?: { ci?: boolean; coverage?: boolean }): string =>
	cmdStr(["tsdx", "test", opts?.ci ? "--ci" : "", opts?.coverage ? "--coverage" : ""]);
const eslintCmd = (opts?: { fix?: boolean }): string => cmdStr(["tsdx", "lint", opts?.fix ? "--fix" : ""]);

//
// CLEAN
//

rmTask("clean", (d) => d(["./assets/lzwolf/exploded", "./coverage", "./dist", "./out", "./src/grammar/parsers"]));

//
// LINT
//

execTask("lint", eslintCmd());
execTask("lint:fix", eslintCmd({ fix: true }));

//
// TEST
//

execTask("test:cyclic", `madge -c --warning --ts-config ./tsconfig.json --extensions js,jsx,ts,tsx --no-spinner ./${srcDir}`);
execTask("test:lint", eslintCmd());
execTask("test:unit", jestCmd());
execTask("test:unit-with-coverage", jestCmd({ coverage: true }));
// TODO: might just be better to stick with .env files otherwise it gets too complicated.
execTask("ci:test:unit", jestCmd({ ci: true }), { env: { CI: "true" } });
execTask("ci:test:unit-with-coverage", jestCmd({ ci: true, coverage: true }), { env: { CI: "true" } });
const baseTestTasks: GulpTask[] = ["test:cyclic", "test:lint"];

const getTestTasks = (opts?: { ci?: boolean; coverage?: boolean }): GulpTask[] => {
	const tasks = [...baseTestTasks];
	if (opts?.ci) {
		tasks.push(opts?.coverage ? "ci:test:unit-with-coverage" : "ci:test:unit");
	} else {
		tasks.push(opts?.coverage ? "test:unit-with-coverage" : "test:unit");
	}
	return tasks;
};

//
// BUILD
//

const grammars: string[] = ["decorate", "jasc-pal", "mapinfo", "wdc-wmc", "xlat"];

const grammarParserTask = (name: string) =>
	inlineTask(`build:grammar:parser:${name}`, async () => {
		const outputPath = `src/grammar/parsers/${name}-parser.ts`;
		await execCmd(`nearleyc src/grammar/${name}.ne -o ${outputPath}`);
		logSuccess(chalk`{green Generated {bold ${outputPath}}}`);
	});
const grammarDiagramTask = (name: string) =>
	execTask(`build:grammar:diagram:${name}`, `nearley-railroad src/grammar/${name}.ne -o public/docs/grammar/${name}.html`);

mkdirpTask("build:grammar:mkdirs", ["src/grammar/parsers", "public/docs/grammar"]);
inlineTask(
	"build:grammar:parsers",
	series(
		parallel(grammars.map((g) => grammarParserTask(g))),
		execTask("build:grammar:parser:prettier", "prettier --write 'src/grammar/parsers/**'")
	)
);
inlineTask(
	"build:grammar:diagrams",
	series(
		parallel(grammars.map((g) => grammarDiagramTask(g))),
		execTask("build:grammar:diagram:prettier", "prettier --write 'public/docs/grammar/**'"),
		execTask("build:grammar:diagrams:git", "git add public/docs/grammar")
	)
);

unzipToFolderTask("test:lzwolf:explode", "assets/lzwolf/lzwolf.pk3", "assets/lzwolf/exploded");

const buildTasks: GulpTask[] = [execTask("build:tsdx", "tsdx build")];

//
// DIST
//

const distTasks: GulpTask[] = [];

//
// TOP-LEVEL TASKS
//

const getTopLevelBuildTasks = (opts?: { ci?: boolean; coverage?: boolean }): GulpTask[] => {
	const tasks: GulpTask[] = [];
	if (!opts?.ci) {
		tasks.push("clean", "pretest");
		tasks.push(opts?.coverage ? "build:test-with-coverage" : "build:test");
	}
	tasks.push("build:compile");
	tasks.push(...distTasks);
	return tasks;
};

task("pretest", series("test:lzwolf:explode", "build:grammar:mkdirs", "build:grammar:parsers"));
task("prepare", series("build:grammar:mkdirs", "build:grammar:parsers", ...buildTasks));

task("test", parallel(...getTestTasks()));
task("build:test", parallel(...getTestTasks()));
task("build:test-with-coverage", parallel(...getTestTasks({ coverage: true })));

task("build:compile", series(...buildTasks));
task("build", series(...getTopLevelBuildTasks()));

//
// CI TOP-LEVEL TASKS, we want to be as granular as possible to split into pipeline stages
//

task("ci:prep", series("pretest"));
task("ci:test", parallel(...getTestTasks({ ci: true, coverage: true })));
task("ci:build", series(...getTopLevelBuildTasks({ ci: true })));
