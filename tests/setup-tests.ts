import { toMatchImageSnapshot } from "jest-image-snapshot";

import "jest-extended";

expect.extend({
	toMatchImageSnapshot,
});
