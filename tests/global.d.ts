declare module "sha256-file" {
	export default function sha256File(filePath: string): string;
}

// eslint-disable-next-line @typescript-eslint/no-namespace
declare module "jest" {
	interface Matchers<R, T> {
		toMatchImageSnapshot(): R;
	}
}
